#!/usr/bin/env python
# coding=utf-8

from argparse import ArgumentParser


ap = ArgumentParser()
ap.add_argument("--xmlrpc", default="https://machineresearch.wordpress.com/xmlrpc.php")
ap.add_argument("--number", type=int, default=100)
args = ap.parse_args()

# Uses https://github.com/maxcutler/python-wordpress-xmlrpc
from wordpress_xmlrpc import Client
from wordpress_xmlrpc.methods import posts

from settings import WP_USER, WP_PASS
import os


client = Client(args.xmlrpc, WP_USER, WP_PASS)
pp = client.call(posts.GetPosts({'number': args.number}))
# posts == [WordPressPost, WordPressPost, ...]
print len(pp)

def filename_for_post (p):
	# t = p.title
	# t = t.replace(" ", "_")
	# t = t.replace("/", "-")
	# longdash = u"–"
	# t = t.replace(longdash, "--")
	t = p.slug
	opath = t+".wordpress.txt"
	return opath

for p in pp:
	print p.slug.encode("utf-8") # title.encode("utf-8")
	opath = filename_for_post(p)
	# print dir(p)
	# print p.slug, p.id, p.link, p.guid, 
	terms = u", ".join([x.name for x in p.terms])
	with open(opath, "w") as f:
		f.write(u"""---
title: {0.title}
slug: {0.slug}
id: {0.id}
link: {0.link}
guid: {0.guid}
status: {0.post_status}
terms: {2}
---
{1}
""".format(p, p.content, terms).encode("utf-8"))
