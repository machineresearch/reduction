---
title: Valentina Vuksic – Implementation attack poetics±politics
slug: valentina-vuksic
id: 98
link: https://machineresearch.wordpress.com/2016/09/26/valentina-vuksic/
guid: https://machineresearch.wordpress.com/2016/09/26/valentina-vuksic/
status: publish
terms: Uncategorized
---
NOTE: Unfortunately Valentina will not be able to join the workshop but will try to contribute remotely.

This is a starting point into some of the physical conditions of computation and its physical vulnerabilities as involuntary by-products. A collection of implementation attacks on physical manifestations of software is gathered as well as unintentional physical phenomena of operative code. They resemble a micro-temporal viewpoint from a purely technological stands. I am interested whether algorhythmic (Miyazaki), sonopoetic (Miyazaki) and sonic (Ernst) qualities can be found in the scenarios described by engineers and to which extent looking from 'within' a very specific – often artificial – technological setting may unravel couplings to larger engineering, economic or political contexts.

Current developments in computing contradict each other regarding the interdependencies of software and its hardware. Virtualisation aims at separating software from the underlying hardware that it runs on while the spreading of little bits of hardware with the Internet of Things binds software and hardware closer together. How the interdependence might look like in specific situations becomes obvious in reports from computer science and engineering about software and hardware intertwining unintentionally (could be virtual machines on a rack in a data centre or a single microcontroller). The academic or non-academic experiments and security tech news serve as directives for approaching the intersections between the presumably immaterial space of software and the presumably material realm of hardware. Both being questioned in media theory for a long time. The reports seem complimentary to technical specs and user guides when it comes to describing a digital device and its operation. They showcase how to interact with technology from 'the side', in a direct manner and unintended by the manufacturers.

Commodity devices might stay vulnerable from a physical perspective to a large extend. Even if countermeasures to a physical leakage become known, they are costly and/or affect performance thus unlikely to be implemented.

Implementation attacks

Implementation attacks are conducted by scientists and hackers alike. They stem from the field of applied cryptanalysis and exploit weaknesses of implementation details. The experiments show unintentional entanglements between a device and its local environment, for example by extracting information out of electromagnetic, acoustic, optical or thermal emissions from a device or injecting energy or commands into them. An increasing number of side channels is being discovered (TBD: REF), statistics and machine learning simplify or improve the extraction (TBD: REF), while the required equipment decreases (TBD: REF). The vulnerabilities of smaller devices like RFID and smartcards were researched first. During the last decade more complex systems like microcontrollers and processors are being examined as well (TBD: REF) while research is expanding into non-cryptographic areas alike (TBD: e.g. REF disassembler; REF power mobile devices).

The imperfect implementation of an electronic device is 'exploited' as a concrete entity in a specific environment instead of being treated as an abstract construction. Thereby exposing fundamental mechanisms and their potential application for debugging or monitoring, as well as their potential misuse by cyber-criminality, legal authorities and even marketing. The experiments or assembled scenarios allow an integrated reading of a technological device via exemplary local appearances and shed a specific perspective on its operation rather than its functionality. Some examples sorted by the year of publication:

Information Leakage from Optical Emanations

Joe Loughry and David a. Umphress, Auburn, 2002 [TBD: LINK]
LED status indicators on data communication equipment, under certain conditions, are shown to carry a modulated optical signal that is significantly correlated with information being processed by the device.
 Optical Fault Induction Attacks

Sergei P. Skorobogatov and Ross J. Anderson, Cambridge, 2003 [TBD: LINK]
Illumination of a target transistor causes it to conduct, thereby inducing a transient fault. Such attacks are practical; they do not even require expensive laser equipment. […] As an illustration of the power of this attack, we developed techniques to set or reset any individual bit of SRAM in a microcontroller. Unless suitable countermeasures are taken, optical probing may also be used to induce errors in cryptographic computations or protocols, and to disrupt the processor’s control flow. It thus provides a powerful extension of existing glitching and fault analysis techniques. This vulnerability may pose a big problem for the industry, similar to those resulting from probing attacks in the mid-1990s and power analysis attacks in the late 1990s.
Hot or Not: Revealing Hidden Services by Their Clock Skew

Steven J. Murdoch, Cambridge, 2006 [TBD: LINK]
We have shown that changes in clock skew, resulting from only modest changes in temperature, can be remotely detected even over tens of router hops. Our experiments show that environmental changes, as well as CPU load, can be inferred through these techniques. However, our primary contribution is to introduce an attack whereby CPU load induced through one communication channel affects clock skew measured through another. This can link a pseudonym to a real identity, even against a system that ensures perfect non-interference when considered in the abstract. We have demonstrated how such attacks could be used against hidden services. We validated our expectations by testing them with the deployed Tor code, not simulations, although on a private network, rather than the publicly accessible one. […] Finally, we proposed how localised temperature changes might aid geolocation.
PRISM: Enabling Personal Verification of Code Integrity, Untampered Execution, and Trusted I / O on Legacy Systems or Human-Verifiable Code Execution

A noteworthy experiment based on logical (software) side channels was published in 2007, the same year as the NSA launched their surveillance program PRISM on the quite. Another concept called PRISM was proposed by Jason Franklin et al., Pittsburgh [TBD: LINK]
A software-only human-verifiable code execution system that temporally separates a legacy computer system into a trusted component and an untrusted component. PRISM enables a user to securely interact with applications by establishing a trusted path and enables personal verification of untampered application execution.
Simple photonic emission analysis of AES

Alexander Schlösser et al., Berlin, 2012 [TBD: LINK]
This work presents a novel low-cost optoelectronic setup for time- and spatially resolved analysis of photonic emissions […]. Observing the backside of ICs, the system captures extremely weak photo-emissions from switching transistors and relates them to code running in the chip.
Current Events: Identifying Webpages by Tapping the Electrical Outlet

Shane S. Clark et al, 2013 [TBD: LINK]
Computers plugged into power outlets leak identifiable information by drawing variable amounts of power when performing different tasks. This work examines the extent to which this side channel leaks private information about web browsing to an observer taking measurements at the power outlet. […] A classifier […] identifies unlabelled power traces of web-page activity from a set of 51 candidates with 99% precision and 99% recall. […] The power trace signatures are robust against several variations including the use of an encrypting VPN, background processes, changes in network location, or even the use of a different computer.
Get Your Hands Off My Laptop: Physical Side-Channel Key-Extraction Attacks on PCs 

Daniel Genkin et at., Tel Aviv, 2014 [TBD: LINK]
The 'ground' electric potential, in many computers, fluctuates in a computation-dependent way. An attacker can measure this signal by touching exposed metal on the computer's chassis with a plain wire, or even with a bare hand. The signal can also be measured at the remote end of Ethernet, VGA or USB cables.
Gyrophone: Recognizing Speech from Gyroscope Signals

Yan Michalevsky et al., Stanford, 2014 [TBD: LINK]
MEMS gyroscopes found on modern smart phones are sufficiently sensitive to measure acoustic signals in the vicinity of the phone. The resulting signals contain only very low-frequency information (< 200Hz). Nevertheless we show, using signal processing and machine learning, that this information is sufficient to identify speaker information and even parse speech.
How TV Ads Silently Ping Commands to Phones

Center for Democracy & Technology, 2015 [TBD: LINK]
Users are also being tracked across devices through the use of ultrasonic inaudible audio beacons, which link devices that are close in proximity to one another. Audio beacons are deployed when a user encounters an ad online and the advertiser drops a cookie on the computer while also playing an ultrasonic audio sound through the use of the speakers on the computer or device. The inaudible code is recognized and received on the other smart device by the software installed on it, which can set the same cookie value and link the two devices.
Remote Command Injection on Modern Smartphones

Chaouki Kasmi and Jose Lopes Esteves, Paris, 2015 [TBD: LINK]
We exploit the principle of front-door coupling on smartphones headphone cables with specific electromagnetic waveforms. We present a smart use of intentional electromagnetic interference […]. As an outcome, we introduce a new silent remote voice command injection technique on modern smartphones.
PowerSpy: Location Tracking Using Mobile Device Power Analysis

Yan Michalevsky et al., Stanford, 2015 [TBD: LINK]
Modern mobile platforms like Android enable applications to read aggregate power usage on the phone. This information is considered harmless and reading it requires no user permission or notification. We show that by simply reading the phone’s aggregate power consumption over a period of a few minutes an application can learn information about the user’s location.
Intentional use of side channels

Manifold experiments are undertaken to use physical emissions intentionally, e.g. for debugging (TBD: REF), malware detection (TBD: REF), software plagiarism detection (TBD: REF), hardware trojan detection (TBD: REF) and even to detect an implementation attack on itself (TBD: REF EM is not non-invasive; REF internal sensors).

Side channels may be applied for software or hardware watermarks (TBD: REF) and covert air-gapped communication channels (TBD: REF GSMem; AirHopper; DiskFiltration; BitWhisper; Schneier A; badBIOS; Sharmir: all-in-one-printer).

Unintentional physical phenomena

Flipping Bits in Memory Without Accessing Them

Yoongu Kim, Pittsburgh, 2014 [TBD: LINK]
as DRAM process technology scales down to smaller dimensions, it becomes more difficult  to prevent DRAM cells from electrically interacting with each other. In this paper, we expose the vulnerability of commodity DRAM chips to disturbance errors. By reading from the same address in DRAM, we show that it is possible to corrupt data in nearby addresses
 Xenon Death Flash

Raspberry Pi Foundation, 2015 [TBD: REF]
 Flashes of high-intensity, long-wave light – so laser pointers or xenon flashes in cameras – cause the device that is responsible for regulating the processor core power (it’s the chip marked U16 in the silkscreening on your Pi 2, between the USB power supply and the HDMI port – you can recognise it because it’s a bit shinier than the components around it) to get confused and make the core voltage drop.
A Loud Sound Just Shut Down a Bank’s Data Center for 10 Hours

Reported by UP2V, 2016 [TBD: REF]
Technical people and customers of ING Bank in Romania found out the effect of sound on hard disks. ING Romania did a fire extinguishing test in the Bucharest datacenter on Saturday September 10 2016.  Staff of ING opened the cylinders to dump the inert gas into the datacenter. However the noise of the gas dump was at such level that the vibration damaged many hard drives resulting in server being unavailable.
USB Kill 2

USB Kill, 2016 [TBD: REF]
The USB Kill collects power from the USB power lines (5V, 1 - 3A) until it reaches ~-240V, upon which it discharges the stored voltage into the USB data lines. This charge / discharge cycle is very rapid and happens multiple times per second. The process of rapid discharging will continue while the device is plugged in, or the device can no longer discharge - that is, the circuit in the host machine is broken.
Wearing out EEPROM memory by writing the same value
On a user forum [TBD: REF]


If I continuously write the same value to the same eeprom location, does it kill the eeprom?
Uint8 __eeprom eeVar;
while(1)
{
eeVar = 0xA5;
}
(Of course i'll never actually use the above code. But just as an illustration) :)

Poetics±politics? (...ongoing...)


Mainly questions here. Hope we can look at one of the papers in detail in the workshop.

Which relationships and dependencies are described in the publications? The listing of possible attack scenarios and countermeasures is interesting as it considers consequences on a larger scale.

Which approaches in media theory and media archaeology could be useful in a close reading of the technical scenarios? Concepts of 'sonicity' and 'algorythms' for example when thinking about time and signals/waves. Possibly other theories from the 'digital materialims' or 'digital turn' discussing material foundations of digital technology.
Sonopoetic knowledge and algorhythmic listening
Shintaro Miyazaki investigates in case studies of algorithms, their cultural, logical and (micro-temporally effective) electrical dimensions alike. (TBD: REF )

'Algorhythms' are time-based technological phenomena and processes, which occur when real matter is controlled by symbolic and logic structures such as instructions written in code. Algorythms are timing-effects of computation. (TBD: REF)

Significance of algorithmic operations is often learned through technological catastrophes. (TBD: REF)

Miyazaki states that listening to audified physical signals generates ‘sonopoetic knowledge’ through the perception of the ‘trans-sonic’ realities of the ‘technosphere’, reflecting the ‘unheard background music’ of our times. These ‘rhythms of the techno-unconsciousness’ are brought forth by detectors or sensors. (TBD: REF)

He asks for a sensitivity towards time-based, microphenomena, towards rhythms and timing to get an idea about ecological interrelations, influences and feedback loops when algorithms interact with each other (TBD: REF).
Sonicity
Ernst 21-24:

Sonicity is where time and technology meet

e.g. precise (electro-)physical micro mo(ve)ment

Technosonic time-mechanisms

sonic tempor(e)alities expressed in terms known from the epistemology of the electromagnetic field

sonosphere and high-electronic media share processuality

sonicity refers to inaudible events in the vibrational (analog) and rhythmic (digital)

names oscillatory events and their mathematically reverse equivalent: the frequency domain as an epistemological object

a special subclass of sonicity is sonics, a non-human embodiment within electronics

sound that originates from electro-technical and techno-mathematical processes

sonicity refers to a temporal nature

sonicity depends on physical or technical embodiments and algorithmic implementation

(techno-) physical events
Ernst 34:
'flow' of time is being replaced by calculated, 'clocked', mathematical time

techno-logical mechanisms are rooted in rhythms and delays, short-time storage and dynamic processing .. thus sonic by nature.



Cited Works (...not complete...)

Clark, Shane S., et al. “Current Events: Identifying Webpages by Tapping the Electrical Outlet.” Computer Security – ESORICS 2013, vol. 8134, 2013, pp. 700–717, doi:10.1007/978-3-642-40203-6.

Franklin, Jason, et al. PRISM : Enabling Personal Verification of Code Integrity , Untampered Execution, and Trusted I / O on Legacy Systems or Human-Verifiable Code Execution. 2007, www.netsec.ethz.ch/publications/papers/PRISM-2007.pdf.

Genkin, Daniel, et al. Get Your Hands Off My Laptop : Attacks on PCs. 2014, pp. 242–260, www.tau.ac.il/~tromer/handsoff.

Kasmi, Chaouki, et al. “Air-Gap Limitations and Bypass Techniques: ‘Command and Control’ Using Smart Electromagnetic Interferences.” THE JOURNAL ON CYBERCRIME & DIGITAL INVESTIGATIONS, vol. 1, no. 1, 2015.

Kasmi, Chaouki, and Jose Lopes Esteves. IEMI Threats for Information Security: Remote Command Injection on Modern Smartphones. Vol. 57, no. 6, 2015, pp. 1752–1755, www.libraryqtlpitkix.onion.link/library/Computing/IEMI Threats for Information Security_ Remote Command Injection on Modern Smartphones_ Chaouki Kasmi_ Jose Lopes Esteves_ 2015.pdf.

Kim, Yoongu, et al. “Flipping Bits in Memory without Accessing Them: An Experimental Study of DRAM Disturbance Errors.” Proceedings of the 41st International Symposium on Computer Architecture (ISCA’14), 2014, pp. 361–372.

Loughry, Joe, and David a. Umphress. “Information Leakage from Optical Emanations.” ACM Transactions on Information and System Security, vol. 5, no. 3, 2002, pp. 262–289.
Mcinnis, Katie. "Cross-Device Tr acking Requires Strong Privacy and Security Standards." Center for Democracy & Technology (CDT), cdt.org/blog/cross-device-tracking-requir es-strong-privacy-and-security-standards.

Michalevsky, Yan, Dan Boneh, et al. “Gyrophone: Recognizing Speech from Gyroscope Signals.” 23rd USENIX Security Symposium (USENIX Security 14), 2014, pp. 1053–1067, www.usenix.org/conference/usenixsecurity14/technical-sessions/presentation/michalevsky.

Michalevsky, Yan, Aaron Schulman, et al. “PowerSpy: Location Tracking Using Mobile Device Power Analysis.” 24th USENIX Security Symposium (USENIX Security 15), USENIX Association, 2015, pp. 785–800, www.usenix.org/conference/usenixsecurity15/technical-sessions/presentation/michalevsky.

Murdoch, Sj. “Hot or Not: Revealing Hidden Services by Their Clock Skew.” Proceedings of the 13th ACM Conference on Computer …, 2006, pp. 27–36, doi:10.1.1.65.9298.

Schlösser, Alexander, et al. “Simple Photonic Emission Analysis of AES.” Journal of Cryptographic Engineering, vol. 3, no. 1, 2013, pp. 3–15, doi:10.1007/s13389-013-0053-7.

Skorobogatov, Sergei. “Semi-Invasive Attacks-a New Approach to Hardware Security Analysis.” Technical Report, University of Cambridge, Computer Laboratory, no. 630, 2005, p. 144, citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.228.2204&rep=rep1&type=pdf.

Skorobogatov, Sergei P., and Ross J. Anderson. “Optical Fault Induction Attacks.” Ches 2003, 2003, pp. 2–12.

Strobel, Daehyun. NOVEL APPLICATIONS FOR SIDE-CHANNEL ANALYSES OF EMBEDDED MICROCONTROLLERS. 2014.
---. “SCANDALee : A Side-ChANnel-Based DisAssembLer Using Local Electromagnetic Emanations.” Date, 2015, pp. 139–144.

Yan, Lin, et al. A Study on Power Side Channels on Mobile Devices. 2015.

