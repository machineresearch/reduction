


$(document).ready(function() 
    { 
    $("table.listing").tablesorter({sortList: [[2,1]]}); 
    // $("table.listing").tablesorter(); 

	function randint (a, b) {
		return Math.floor(a + (Math.random() * (b-a+1)));
	}
	function colortitle () {
		var title = document.getElementById("title"),
			text = title.innerHTML,
			p, l, span;

		function shuffle () {
			title.innerHTML = "";
			p = 0;
			while (p < text.length) {
				l = randint(1, 4);
				span = document.createElement("span");
				span.innerHTML = text.substring(p, p+l);
				if (randint(1, 3) != 3) {
					span.style.background = "hsl("+randint(0, 360)+",100%,80%)";
				}
				title.appendChild(span);
				p += l;
			}
		}
		shuffle();
		// window.setInterval(shuffle, 10000);
	}
	colortitle();

	}
);

var newpadbutton = document.getElementById("newpadbutton");
newpadbutton.addEventListener("click", function (e) {
	var elt = document.getElementById("namefilterinput"),
		newpadname = elt.value,
		padbase = document.querySelector("td.versions a").href;
	newpadname = newpadname.replace(/^\s*/g, "");
	newpadname = newpadname.replace(/\s*$/g, "");
	elt.value = newpadname;
	padbase = padbase.replace(/\/[^/]+$/, "");
	if (!newpadname) { alert("type the pad name, then click 'go'")}
	else {
		var newpadhref = padbase + "/" + encodeURIComponent(newpadname);
		// console.log("goto", newpadhref);
		window.location = newpadhref;
	};
	e.preventDefault();
})
var namefilter = (function (opts) {
	var timeout_id = null,
		filter_value = '',
		delay = (opts && opts.delay) || 1000;
	function update() {
		// console.log("update", filter_value);
		var pat = new RegExp(filter_value, "i");
		$("tbody tr").each(function () {
			var n = $(".name", this).text();
			// console.log("n", n);
			if (filter_value == "" || n.match(pat) !== null) {
				$(this).show();
			} else {
				$(this).hide();
			}
		})
	}
	var ret = function (val) {
		filter_value = val;
		if (timeout_id !== null) {
			window.clearTimeout(timeout_id);
			timeout_id = null;
		}
		timeout_id = window.setTimeout(update, delay)
	}
	return ret;
})();

$("#namefilterinput").bind("keyup", function (e) { namefilter($(this).val()); })


