#!/usr/bin/env python
# encoding=utf8  

 #    Copyright (C) 2016 Constant, Algolit
 #    This program is free software: you can redistribute it and/or modify
 #    it under the terms of the GNU General Public License as published by
 #    the Free Software Foundation, either version 3 of the License, or
 #    (at your option) any later version.

 #    This program is distributed in the hope that it will be useful,
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

from __future__ import division
import sys  

from argparse import ArgumentParser
ap = ArgumentParser('''
This script goes through the input text word by word. Every duplicate word and its subsequent occurence is removed, until the desired reduction is reached.

Disappearance is inspired by a script by the same name developed by Stephanie Villayphiou and Alex Leray, that takes a .srt file as an input. It appeared for the first time in the context of TimedText, a workshop that considered writing, reading and listening as parallel but interacting tracks (Constant, 2010). The script was developed for and applied to a transcription of an interview with dance-maker, improvisational performer, and collaborative artist Lisa Nelson.

http://activearchives.org/wiki/Disappearance
http://activearchives.org/wiki/Kitchen_table_workshop
http://olga0.oralsite.be/oralsite/pages/What's_the_Score_Publication/
''')
args = ap.parse_args()

punctuation = [" ;''?:,\n()!.\").”-"]
used_words = {}
cnt = 0
for line in sys.stdin:
    words = line.split(" ")
    for word in words:
        if word:
            if word not in punctuation:
                if word in used_words:
                    length = len(word)
                    spaces = length*"&nbsp;"
                    sys.stdout.write(spaces)
                else:
                    used_words[word] = 1
                    sys.stdout.write(word)
                    cnt += 1
                    if cnt >= 1000:
                        sys.exit()
        sys.stdout.write(" ")
    sys.stdout.write("\n")
