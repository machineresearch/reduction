#!/usr/bin/python

import sys
import re

from argparse import ArgumentParser
ap = ArgumentParser('''
++++++++++++++++++++++++++++++

     /$$$$$$$$ /$$$$$$ 
    |__  $$__//$$__  $$
       | $$  | $$  \ $$
       | $$  | $$$$$$$$
       | $$  | $$__  $$
       | $$  | $$  | $$
       | $$  | $$  | $$
       |__/  |__/  |__/
                       
T H E    A C R O N Y M I Z E R
++++++++++++++++++++++++++++++

Ever feel that your text is too verbose? Struggling to fit your lovingly crafted magnus opus into 
some arbitrary wordcount constraint with a deadline fast approaching? Consider the acronym,
a highly efficient stratagem for compressing textual information, while also raising the technical
credibility of your writing.\n\nThe Acronymizer (TA) finds repetitive phrasings in a text, and 
builds a suggested glossary which you would do well to consider adding as an appendix to your work!
''')

args = ap.parse_args()
corpus = sys.stdin

phrases = []
multiples = []

glossary = {}

def ampersander(txt):
    return re.sub(r'\sAND\s', ' & ', txt, flags=re.IGNORECASE)

def mkPhraseList(numWords):
    cleanText = re.sub('[!@#$():,.]', '', corpus.read().strip().decode('utf-8'))
    cleanText = ampersander(cleanText)
    cleanText = cleanText.split(' ')
    for i in range(len(cleanText)-numWords):
        phrase = ''
        for j in range(numWords):
            phrase = phrase + cleanText[i+j]+' '
        phrases.append(phrase)

def findMultiples():
    uniqueWords = set(phrases)
    phraseFrequency = sorted([(word, phrases.count(word)) for word in uniqueWords])
    for p,f in phraseFrequency:
        if f > 1:
            multiples.append([p,f])
    return multiples

def createGlossary():
    for p,f in multiples:
        acronym = ''
        for w in p.split():
            acronym = acronym+w[0][0].upper()
        
        glossary[acronym] = p 

mkPhraseList(3)
findMultiples()
createGlossary()

sys.stdout.write('-----\n')
sys.stdout.write('#GLOSSARY\n\n')

for g in sorted(glossary):
    if(len(g.split())>0):
        output = '{} : {}\n'.format(g, glossary[g].upper())
        output.encode('utf-8')
        sys.stdout.write(output)
        sys.stdout.write('')
