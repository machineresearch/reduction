#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from argparse import ArgumentParser
import sys
import nltk
from pattern.en import sentiment



ap = ArgumentParser('''
Input texts are checked against polarity scores for used adjectives. When the score is lower than 0.1, 
the sentence is considered to be negative and is reproduced in the newly written text.
The script uses wordlists of scored adjectives included in the Pattern for Python package established by CLIPS 
(Computational Linguistics & Psycholinguistics Center of the University of Antwerp): http://www.clips.ua.ac.be/pattern. 
The scores of the words are given by students of UA who annotated the Dutch corpus. These annotations were then exported to automated English translations. 
Pattern for Python is widely used by companies to get an idea of how their products are received in the world of social networks.
This Python library was one of the research topics at Cqrrelations, a worksession organised by Constant in January 2015: http://www.cqrrelations.constantvzw.org

''')
args = ap.parse_args()


for line in sys.stdin:
	line = line.decode('utf-8')
	# Split text into sentences with help of nltk
	sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
	sentences = sent_tokenizer.tokenize(line)

# select subjective sentences
negatives = []
for s in sentences:
	scores = sentiment(s) 
	polarity = list(scores)[0]
	if polarity < -0.1:
		negatives.append(s)

sys.stdout.write(" ".join(negatives).encode('utf-8'))
