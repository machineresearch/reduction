#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
ap = ArgumentParser("""
Keep your summary in style!

Using Markov Chains, texts are rewritten and reduced based on the word pairs (n-grams) present in the original text. Markov chains are widely used in attempts to pass messages through spam filters. Anti-spam software uses Bayesian analysis that is based on the Markov chain to keep up with spam techniques. The Markov generator begins by organizing the words of a source text stream into a dictionary, gathering all possible words that follow each chunk into a list. Then the Markov generator begins recomposing sentences by randomly picking a starting chunk, and choosing a third word that follows this pair. The chain is then shifted one word to the right and another lookup takes place and so on until the document is complete. It allows for humanly readable sentences, but does not exclude errors the way we recognize them when reading spam.

This filter is based on a workshop by Sebastian Luetgert in the context of "(Re-)Constructing Authorship" (Stuttgart, 2015). Markov Chain was also performed as a reading/writing game by Brendan Howell, Catherine Lenoble and An Mertens, by then members of Algolit, the Constant working group around F/LOSS literature, texts & code

http://algolit.constantvzw.org
https://en.wikipedia.org/wiki/Markov_chain
""")
args = ap.parse_args()

import os, sys, json, random, time

# set the limit; this can be any size (also longer!)
length = 1000

# amount of n-grams
n = 2

# read some text
text = sys.stdin
text = text.read()

# calculations
words = text.replace('\n', ' \n').replace('  ', ' ').split(' ')
data = {}
for i, word in enumerate(words):
	if i >= n - 1: 
		key = ' '.join(words[i - (n - 1):i])
		data[key] = data.get(key, []) + [word]
# output text
words = random.choice(list(data.keys())).split(' ')

x = 1
for x in range(0, length):
    try:
        words.append(random.choice(data[' '.join(words)]))
        sys.stdout.write(words[n-1].decode('utf-8').encode('utf-8')+' ')
    except KeyError:
        pass
    # for char in words[n - 1] + ' ':
    #     print(char, end='')
    #     sys.stdout.flush()
    #     #time.sleep(0.050)
    words = words[1:]
    x = +1

sys.stdout.write(".\n\n")
sys.exit()

