#!/usr/bin/env python
# encoding=utf8

from argparse import ArgumentParser
import os,re,sys,subprocess

ap = ArgumentParser('''


                                                           _/7
                                                          (o o)     
                                                      ooO--(_)--Ooo         
 __      another release of       ___    __               WTFPL etc          
|  |   ___ ___ ___ _ _ ___    ___|  _|  |  |   ___ ___ ___ ___ _ _ 
|  |__| -_| .'| . | | | -_|  | . |  _|  |  |__| -_| . | .'|  _| | |
|_____|___|__,|_  |___|___|  |___|_|    |_____|___|_  |__,|___|_  |
              |___|                 2k16          |___|       |___|


Clicking on any of the external links in your text sets in motion a request 
for information which travels along a specific route to reach the desired website. 

The route travels across various networks, owned by the different service providers 
that make up the internet.

The way the route will be chosen depends on your Internet Service Provider, as a
consequence of conditions ranging from your geographical location to the specific 
commercial deals existing between networks - the so-called 'peering agreements'.

This filter adds the information of what networks are accessed to reach a specific
resource, producing a metadata of sorts which is added to your citations.


It is based on two tools that are used to take measurements of computer networks:
Traceroute and Whois.

Traceroute shows the routed path across the internet between your own network 
and a given destination. This path is shown by listing the Internet Protocol 
address of each router on the way.

Whois is a tool to look up ownership information about the Domain Name System. 
In order to get a domain name one has to provide contact details to the domain
registrar - this information is publicly accessible.

Whereas traceroute shows the abstract logical adresses of each node in the network, 
whois turns this information into a story of a network of networks, with different
 owners, material conditions and legacies.

The result of this script will be different depending on the location from where it 
is launched. 

If something doesn't work, blame the network not the script and try again.

''')

args = ap.parse_args()

text = sys.stdin
text = text.read()
destinations=[]
listnodes=[]
urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
for url in urls:
#   sys.stdout.write(url+'\n')
    destinations.append(url)

destinns=set(destinations)                                

for dests in destinns:
    #print dests
    dest=dests.split('//')[1].split('/')[0]
    trace = subprocess.check_output(['lft', '-SA', dest])

    trace = trace.decode("utf-8")
    AS = []
    for i in trace.splitlines():
        if i.startswith('TTL'):
            pass
        else:
            if not i.startswith('**'):
                try:
                    i=i.split('[')[1].split(']')[0]
                    AS.append(i)
                except:
                    pass
    a = set(AS)
    listnodes=[]
    for asss in a:
        if not asss == 'AS?':
           #    print asss
            process = subprocess.Popen(['whois', 'AS'+asss], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            aswho, err = process.communicate()
            for line in aswho.splitlines():
                    if ("OrgName" in line) or ('org-name' in line):
                        a = line.split()[1:]
                        listnodes.append(' '.join(a))
    if (text.find(dests)!=-1):
        index=text.index(dests)+len(dests)
        text=text[:index]+ ' (During the workshop in Bruxelles this resource was reached via the following networks: '+'; '.join(listnodes)+'. )' +text[index:]

print text
