#!/usr/bin/env python
# -*- coding: utf-8 -*-


 #    Copyright (C) 2016 Constant, Algolit
 #    This program is free software: you can redistribute it and/or modify
 #    it under the terms of the GNU General Public License as published by
 #    the Free Software Foundation, either version 3 of the License, or
 #    (at your option) any later version.

 #    This program is distributed in the hope that it will be useful,
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

from __future__ import division
import sys  

from argparse import ArgumentParser
ap = ArgumentParser('''
Input texts are checked against occurences of certain words included in a list of "stopwords" established by NLTK (Natural Language Toolkit). These words are then removed.

In data mining, text processing and machine learning, these so-called high frequency words are filtered out before or after natural language data is processed. Relational words such as 'the', 'is', 'at', 'which', and 'on' are considered redundant because they are too frequent, and meaningless once the word order is removed.

http://www.ranks.nl/stopwords

The list of stopwords being used is visible here: http://etherbox.local/var/www/filters/english
''')
args = ap.parse_args()

stops = set()
with open("english", "rt") as source:
	for line in source:
		line = line.strip()
		stops.add(line)

for line in sys.stdin:
	words = line.split(" ")
	filtered_words = [word for word in words if word not in stops]
	newtext = " ".join(filtered_words)
	sys.stdout.write(newtext)




