from nltk.corpus import wordnet as wn
import sys
import random
from random import choice
from argparse import ArgumentParser
ap = ArgumentParser('''
++++++++++++++++++++++++++++++                     
T H E    S Y N O N Y M I Z E R

Finds synonyms for choice words
in your text, disrupts your writing
stylistics, renders you pseudonymous
and synonymous.

hurray!
++++++++++++++++++++++++++++++
''')

args = ap.parse_args()

corpus = sys.stdin

corpus = corpus.read().decode('utf-8')

ignoreStrings = ["you", "or", "me", "be", "a", "Oh", "I", "no", "to", "on", "As", "as", "was", "it"]

def uppercase(s):
    if(len(s)>1):
        return s[0].upper() + s[1:]+'.'
    else:
        return ''

def getSyns(line):
    sentence = []
    for word in line.split(' '):
        
        curr_synset = []
        if word in ignoreStrings:
            sentence.append(word)
        else:
            for ss in wn.synsets(word):
                ss = ss.name().split(".")[0]
                curr_synset.append(ss)
            
            if(len(curr_synset)>0): 
                word = choice(curr_synset)
                word = word.replace('_', ' ')
                sentence.append(word)
    s = ' '.join(sentence)
    output = uppercase(s)
    sys.stdout.write(output+'\n')

for l in corpus.split('.'):
    getSyns(l)
