#!/usr/bin/env python
# encoding=utf8  

 #    Copyright (C) 2016 Constant, Algolit
 #    This program is free software: you can redistribute it and/or modify
 #    it under the terms of the GNU General Public License as published by
 #    the Free Software Foundation, either version 3 of the License, or
 #    (at your option) any later version.

 #    This program is distributed in the hope that it will be useful,
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

from __future__ import division
import sys  


from argparse import ArgumentParser
ap = ArgumentParser("""
""")
args = ap.parse_args()

used_words = {}

for line in sys.stdin:
    words = line.split(" ")
    for word in words:
        word = word.strip(" ;''?:,()!.\").”-")
        sys.stdout.write(word)



