---
title: Nicolas Malevé – Machine pedagogies
---

http://www.papert.org/articles/freire/freirePart2.html
( In the context of the Machine Learning workshop in Bruxelles, this resource was reached via the following networks: Proximus NV; RIPE Network Coordination Centre; Telia Company AB; Amazon.com, Inc.; Amazon.com, Inc.. )

http://mturkpublic.s3.amazonaws.com/docs/MTURK_BP.pdf
( In the context of the Machine Learning workshop in Bruxelles, this resource was reached via the following networks: Proximus NV; RIPE Network Coordination Centre; Telia Company AB; Amazon.com, Inc.. )

http://www.ibmbigdatahub.com/blog/ground-truth-agile-machine-learning
( In the context of the Machine Learning workshop in Bruxelles, this resource was reached via the following networks: Proximus NV; DSCI Corporation; Level 3 Communications, Inc.. )

http://image-net.org/
( In the context of the Machine Learning workshop in Bruxelles, this resource was reached via the following networks: Proximus NV; Stanford University; Hurricane Electric, Inc.. )

https://machineresearch.wordpress.com/2016/09/26/nicolas-maleve/
( In the context of the Machine Learning workshop in Bruxelles, this resource was reached via the following networks: Proximus NV; Belgacom International Carrier Services SA. )

https://en.wikipedia.org/wiki/Supervised_learning
( In the context of the Machine Learning workshop in Bruxelles, this resource was reached via the following networks: Proximus NV; RIPE Network Coordination Centre; Telia Company AB; Wikimedia Foundation, Inc.. )

