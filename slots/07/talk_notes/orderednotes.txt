
☎ Antoinette Rouvoy
★ COLLECTING NOTES BY EVERYONE
♀ Antoinette Rouvroy
☎Algorithmic Governmentality
★Optimized society 
☎The art of not changing the world
♂ Antoinette Rouvroy: “Algorithmic governmentality: the ‘art’ of not changing the world"
♀algorithmic governmentality - an art of not changing the world
☎escapes or senses
★We don't believe promise but services are so present that it changes us
✓ Welcome to Etherpad!
☎replacemnet of all we call categories, we are predisposed to management world
✔Agovernmentality that escapes the senses
★We're indexed,  I change my behaviour 
♀a description of phenomena that escapes the senses
☎to be replced by digital reality by human intervention
♂Algorithmic reality not sensible to human perception
☎blindness, make singularity of life…
★Radical competition between fragments of Algoritmic persons 
♀"immunisation from life" (against the future)
☎aura of impartiality…
★Raw data is cultural good 
☎tries to map contradictions
♀     against uncertainty as such 
✓ This pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!
★Is very much cooked 
☎new way of making the world predictable…. preemptive, available for action
♂Algorithmic govermentality not making meaning, but creating probabilities
✔Areplacement of categories with something that is invisible but arguablyimmanent with the world
☎vision of algo, creates new objects and spaces 
★Anonymous data,  cleaning data,  formatted to needs 
♀desire for unmediated relationship to the world (non-representational)
☎A.G. process of
★Data prior of its processing does not exist 
☎art not changing world, closing digital on itself
♀ crises of representation
♂Algorithmic governmentality immune system / immunization from life
☎anything that is not computational
★Totalizing concepts 
☛What  is the 'potential space' of algorithmic governmentality: "the present of AG is a poor present, it is already dead because it is not trembling with potentiality"
✓ To prevent your pad from appearing in the archive, put the word __NOPUBLISH__ (including the surrounding double underscores) anywhere in this pad. Changes will be reflected after the next archive update.
☎it is immumization from life
✔Anew world of making the world prepared for action and speculation – a new spacefor speculation and action
♀          because everything is present
★It is not Totalizing,  only contains what is digital 
☎abstraction of digital world
♂The ‘art of not changing the world'
★3 things will always escape : 
☎raw data: erasure, of singularity
♀attempts truth-regime (uncontestable), like the law also posits a factive truth
☎being something else than the world…
★Utopias from past vs shrinked bog data reality 
♀big data suppresses alterity
☎what defines living is alteration
✓ Warning: DirtyDB is used. This is fine for testing but not recommended for production. -- To suppress these warning messages change suppressErrorsInPadText to true in your settings.json
★Resistance to highly structured organisation,  organic lives are occasions for bifurcation 
☎livig is to be altered
♂Uncertainty at the core of algorithmic governmentality??
✔Animmunesystem of the digital, an immunisation from life, abstraction of thedigital from the physical world
♀Foucault - avowal
☎normativity to events in the world, completely immanent
★Speaking bodies because we're incomplete,  not entirely in the present,  because of language 
☎Life used to be unpredictable, now it is predictable
★It disconnects us from ourselves,  reason why wee uncertain 
♀     commitment
☎used to be uncertainity as such
♂Algorithmic worlding speaking for themselves – and for us
★Reason why judge can decide even if not clear - revision of our own production  (law)
☎things respresented by data
✓ Antoinette Rouvroy
♀government without subject, a radical shift as data speaks for us
☎science and things become the same
✔Rawdata as something that replaces the world
★Algoritmic governmentality is dead 
☎Big Data: crisis of representation
♀raw data pretends to be a second skin of the world
★Ethics available because we can doubt and we fail 
♂Algorithmic dispositifs disposes us from truthtelling (cf. Foucault) – in big data we don’t have to utter anything as there is no subject
☎we would like to be able to grasp the world without mediation, the world speaks with degree zero writing of 1 and 0
☎speaking for us (9:00)
★ SEDA presentation 
♀but is not raw but cooked
☎Governmentality is Foucauldian
☛We want to grasp the world without mediation - this is big data. Assumes the world speaks about itself in the categories laid out. 
✓algorithmic governmentality
★ (notes An)
☎opposite of truth regime…
♂Possibility of critique disappears
✔Anormativity (Canguilheim) that is not different from the world
♀data seems to sugest that the world has spoken
★The cloud 
☎particular force… by the wish to know
☎naming things, constitutes the law. But now, algorithms, we don’t have to testify, we are free not to tell anything…
♀things that begin to speak for themeselves and for us
★Ideology and practice of.big data 
☎subjectification:through avowal: process of subjectifcation (12:42)
♂Algorithmic profiling becomes a second skin to oneself, almost impossible to escape
★Suspicious why we talk about it now 
✓"immunisation from life" (against the future)
☎algorithmic government: is w/o gov’t
♀that signs and symbols become the same is a symptom of a crisis of representation
☎body w/o event, alteration
★History of privacy and big data 
✔Indistinctionbtw things and data – signs and things become the same
☎disappear: the ability of critique
♀     
★There is no software only services
♂Dif. between big data as ideology and as practice
☎data mining, data visualisation
♀removing the distance of real life to the norm
☎classical statistics: gave more or less a representation that could be criticized
★crisis of software ever since it was born 
✓     against uncertainty as such 
☎(not enough data, not enough)
★Use services in the cloud 
♀no selectivity is evident, even noise is incorporated in big data
☎Big data: no selectivity: even noise is taken up and considered
♂Naturalizing whatever is captured as reality – appearing as matter of fact
✔è crisis ofrepresentation – we cannot bear differance anymore – the world speaks foritself
★Software industry moved - you will constantly come to us 
☎profiles… not willing to be profiled
♀objectivising the reproduction of the current state of affairs, making it impossible to contest
☎raw data pretends to be second skin of the world
★Waterfall model for software development grew into individual as part of software process = agile programming 
☎optimisation of everything (regime where we now live)
★Optimize software production I such a way that had as side effect, big data 
☛"How does AG exist alongside broken, corruptible, totalizing forces -when in fact things are not always all on the grid and not everything is digitized. 
♀if this is the art of not changing the world - of closing the present as the spece of alteration - then what is the art of changing the world?
♂“The world has spoken” – why not allow digitalizing of life construct reality?
✓desire for unmediated relationship to the world (non-representational)
☎big data: ideology and practice
☎amnesia of data (forget amterial conditions)
★Privacy, discriminatie and freedom as only lenses 
✔è a new truthregime? In fact the opposite. Dispositifs som ikke tillader vidner, we are freenot to tell anything, no subjectivation
♀reenactment is a space for potential 
☎practice: it allows us naturalise whatever is captured
★Why think not using FB is political act 
☎big data appear to store facts..
♂Aim is not truth but optimization
♀‘signature’ of a terrorist in a database
★What is the project : bring down the cloud
☎conditions, whoever knows them, changes them
✓ crises of representation
☎not changing state of affairs: recommendation systems, data mining, naturalise sterotypes in society (this art of not changing world)
★How? If it fills up the cloud, that's not the project 
♀ Seda Gürses
☎it pretends not to be constructed….
♂Event is defeated – whenever there is a counter-actualization of an event, the data of the event is diffracted and extracted from the event
✔A government without subject – evolving in real time, noevent, no interruption. 
★95% thinks there is no histories 
☎algorithms rule the world they are like gods…
♀There is no software, there are only services that are now linked to agile development
☎not a truth regime, doesn’t have an event in order to arise!
★Maybe look at different things ex history of sewers / shit (you need privacy to be able to bury your shit in ground, way to clean up french language )
☎liquidation of the event (best to prevent change)
♀what we learn from histories - crisi of softwrae that led to the field of software engineering
✓          because everything is present
★Microsoft bought LinkedIn using debt while they have massive cash 
♂Alteration at the core of human life – algorithmic governmentality surpressing alteration
☎digital marketing (personalised ad you receive things, don’t clicking is also information
★Industry of banks and co that invested in development of cloud and now they have to fill it 
☎drones: missiles are sent by data mining algos.. not people who are detected….
♀what gives rise to big data? how different poetntia spaces develop? why we talk about the things we do, what can we do?
✔When signs and things become the same thing the space forcriticism disappears. Vs classical statistics, som kunne kritiseres. 
☎signature.. instead
★Data as nuclear waste 
♀in other other words, how to bring down the cloud? 
☎no way to validify the model
♂Everything is always present – nothing can be re-presented
★Radioactive toothpaste example 
☎immance of the algorithm…..
☛Four things escape and always escape digitalisation and these are the spaces to hold on to as resistance:
✓attempts truth-r
♀suspicion is required
☎suppressing alteration
★Dns disrupted by machines of Internet of things 
☎Big Data, Raw Data
♂Virtual potentiality is removed
✔Der er ingen udvælgelse (ideelt), selv støjen tager med,dvs. den tager alt med, selvom det singulære. 
★Revolution with baby phones 
♀we need to tae decisions on uncertain futures
☎38:00
★Cubersecurity conceptualized by the state 
☎Raw Data is very much cooked
♀perhaps waht we require is a new social contract
☎de anonymizing data
✓gime (uncontestable)
★China, Russia and Iran were ways to push things forward 
♂Law is life – algorithmic governmentality is death
☎de contextualising data
♀ Discussion
★Dependence of companies 
☎what is a data ( is result of the processing) you translate the physical universe
✔Ifbig data is the world, why criticise? Men stor forskel mellem big data ideologiog praksis. 
☎data is not voluntary transcriptions…
★AI ethics 
♀rawification of data - decontextualising
☎not everything is digital data
♂Need to invent new law of potential spaces and potential objects
★Or big data or ethics 
✓Foucault - avowal
☎3 things to escape:
♀language connects us to others but disconnects us form the self
☎recalcitrance
★Ethics is space of critique between things and representación of things 
☎things exist in digital form, so that the happen sometimes, 
♀     raw revises itself (autopoietic)
★ DISCUSSION
♂Projects without predictions rather than predictions without projects – project! (etym. = projection, throw forth) (projects, objects, subjects – subject as project, a matter of duration)
✔Bigdata ideology er en slags amnesi – glemmer materielle betingelser – 
☎the past (traces in art and utopias)
★ (Notes An)
☎the future (against of excessive organisation)
♀are there other potentialities here of acting with machines - inter-subjective relations or trans-individuation? 
☛- physical things - the fact of bodies and organic life that is unpredictable
✓     commitment
☎organic life (we have bodies)
★Ethics : seeing what is behind the discourse 
♂Ethics is inherent in human beings only because we fail
☎we are speaking bodies (we are incomplete) which is why we speak to each other
♀imhoff
★Difference between ethics and politics? 
☎by nature we are schizophrenic, why we are uncertain beings
✔Facts:Canguilheim: Facts appears to forget their conditions – 
♀laporte's history of shit - a good reference to think about how language is cleansed in order to reinforce the power of the sovereign
☎judges: has to decide…revision of law, it’s alive (full of bugs and corrupt, yes)
★Ethics can be applied by politicians for ex sign Ceta 
☎algorithmic governmentality is dead (computers fail a programme, not really fixed)
♂Facing the fact that we can be wrong is ethical
★Having courage to bifurcate instead of following the calculation / the program 
✓government without subject
♀and the management of waste and the implications for how we think about infrastructure
☎ethical beings….
★Ethics used to take conflict out 
☎Transindividuation:
♀who is this us? 
☎opens up a radically new space and objects…
★Is rule based ethics 
♂What would ethics be in a system? Can a system be ethical?
✔Istedet for truth -> operationality – a truth that doesn’t depend on anyevent – the liquidation of any kind of event
☎we haven’t invented these politics for space…
♀AR: the legal subject who acts as pure immanance with rights to be forgotten and to disobey
★Way of keeping uncertainty away 
☎ Seda Gurses
✓     
☎Privacy and Engineering, computer scientist
★Ethics takes risk to be wrong 
♀- an art of not changing the world
♂Question of accountability
☎Suspicion
★Which discourse on big data tries to keep us right and safe 
☎why is Big Data unprecented?
♀a description of phenomena that escapes the senses
✔Thesuppression of alteration – 
★Experiment with million people and fail in ethical way? 
☎Dutch Ministerie to her: what you said about the last 30-40 years….is invalid, b/c we now have Big Data
☎There is no software only services.
★Question of accountability 
☛- utopias we had which didn't find a place in any present - 
♀e
♂ME: would create a system of precarity in which the individual doesn’t wish to take any risk because it all falls back in the someone – go back to agency
✓removing the distance of real life to the norm
☎Fundamental change software (shrink wrap software) to services
★Cie speaks through human persons 
☎The Crisis of Software
♀, like the law also posits a factive truth
☎(field constantly changes)
★Solon Barocas local optimum 
♂Is there a difference between making organisations accountable and making individuals accountable?
✔Putsus in a radical competition
☎We use the Cloud: Google.docs, Etherpad (part of another logic)
♀big data suppresses alterity
★Reroute trafic, to places where there is uncertainty 
☎Software: used to sell it. Now they keep it, sell services
✓objectivising the reproduction of the current state of affairs, making it impossible to contest
★Cfr autonomous cars not ethical now because people cannot be accountable or justify accident. There is no space to discuss this. Car will not hesitate 
☎Waterfall model.
♀, a radical shift as data speaks for us
☎Looking at software engineering practice…
♂Human rights in an era of algorithmic governmentality:
★Ethics needs a scene and time. 
♀raw data pretends to be a second skin of the world
☎Why are we talking about big data, why algorithms?
✔Rawdata as oxymoron – raw data as the process of the rawification of data – 
★Quantification was already there : 
☎privacy, security, etc.
♀but is not raw but cooked
☎Why do we use these lenses for critique?
✓‘signature’ of a terrorist in a database
★Would you kill 10 people or 1? 
♂The right to fabulate, the right to be forgotten, the right to express/account for oneself
☎privacy, discrimination and freedom? as responses?
★Symptomatic 
♀data seems to sugest that the world has spoken
☎We are going to bring down the cloud!!!!
☎Obfuscation of data (fills up teh cloud) that is not the tool…..
★See and register where shifts occur 
♀things that begin to speak for themeselves and for us
♂ Seda Gürses: “Histories of big data"
✔Bigdata is only about the digital:
☎Order: History of Objectivity
★History of crash testing moment where mathematics started to be understood as risk management 
☛- dreams of the future -  tenancy f life to be recalcitrant to organisation  - that;s what 
✓ Seda Gürses
☎Statistics
♀that signs and symbols become the same is a symptom of a crisis of representation
★Think differently about it on large scale 
☎Data
☎not infrastructure of railroads, but sewers!!!! History of shit (dig in the ground)
♂Genealogy of big data – thinking about the different stories in order to understand why we suddenly talk about algorithms, big data, etc.
★Ethics as accountability and how does that apply to company? 
♀no selectivity is evident, even noise is incorporated in big data
☎Today: Microsoft (third quarter spike)
★What is role of FB in stopping Trump 
✔thingsthat are not digital: things that have not happened (e.g utopias),bodies/organic life, the fact we are not completely contained, sprogetindsætter forskel -> tvivl – 
☎They bought Linked-In using debt…..
♀if this is the art of not changing the world - of closing the present as the spece of alteration - then what is the art of changing the world?
✓ Discussion
☎History of Shit: is about French language, demise and also the protection of Soveriegn
★It is not about 1 thing. It is being produced by different factors 
♂From shrink-wrap software to services – end of software
☎History of time-sharing, computing, and Waste…. wasted time was for debugging…
♀reenactment is a space for potential 
★Agile software production 
☎Data: is Nuclear Waste, it’s going to be around for a long time…
★Bring back sensors, asking questions to people and map big data onto that 
☎1:07
♀There is no software, there are only services that are now linked to agile development
♂Instead of selling produced software and distributing it to people, the companies keeps the software and people come to them
✔Weneed a new law, politics and culture of the potential – reopen the space forimagination
☎DNS: disrupted by bots (if IoT devices) baby monitor, consumer devices
★Taking process backwards :-)
✓rawification of data - decontextualising
☎Was it Russia or China? (he says targeting US)
♀what we learn from histories - crisi of softwrae that led to the field of software engineering
★Nice exercise 
☎cyber security discourse (needs to be unraveled)
♀what gives rise to big data? how different poetntia spaces develop? why we talk about the things we do, what can we do?
☎ethics or big data
★Individuals could claim their data 
♂Data streams created through services
☎ethics is a space of critique…. notion of situated practices….
★Run traditional and big data censors to see the difference 
♀in other other words, how to bring down the cloud? 
✔Relationsbtw dababases and big data – 
☎managerialisation….
☛- If we were really present and complete we would not talk to each other;we are separated from ourselves through language anyway; 
✓language connects us to others but disconnects us form the self
★Polyphonic perspective 
☎what is the difference b/w ethics and politics
♂Bring down the cloud
♀suspicion is required
☎duty to govern…. to diverge and bifurcate
★Debunk the myth of something 
☎follow calculation, you are not ethical….
♀we need to tae decisions on uncertain futures
★Sts archeologies 
☎optimisation is not ethical
♂Bringing down the cloud can only be down by tracing the genealogies of its existence
✔agiledeveloping -> the disappearance of the subject-user -> becomes somebodythat is constantly in the development process, not somebody who speaks
☎Question of accountability!!!!
★Protocol histories 
✓     raw revises itself (autopoietic)
♀perhaps waht we require is a new social contract
☎you can have systems of reflexitivity done my humans
★Philip agri 2 stories of surveillance 
☎Census model: 2021 in UK
♀re there other potentialities here of acting with machines - inter-subjective relations or trans-individuation? 
★http://www.tandfonline.com/doi/abs/10.1080/01972243.1994.9960162
☎not just administrative, but asking people questions. This woud add robustness to the data (Rouvroy)
♂“History of shit” – what is proper language, who is able to speak, etc.
☎Phil Agre! (Seda mentioned his article I downloaded)
♀laporte's history of shit - a good reference to think about how language is cleansed in order to reinforce the power of the sovereign
★Ted  Byfield,  History of visualisations 
✔Bringdown the cloud, if your project fills up the cloud, it is not the project – 
☎Ted Biefeld: looking at the History of vVsualisation!!!!!
✓a
★Genealogies 
☎Rethink a new social contract! (Rouvroy)
♀and the management of waste and the implications for how we think about infrastructure
♂Waste: what is defined as waste? Excess.
☎Cloud is just other people’s hard drives (Seda thought Søren said ‘house wives)
★Present augmented to the future 
☎Rouvroy: algoithms (cannot do parallel calculations, it’s not performative, it’s not a language)
♀who is this us? 
★If you're terrorist today big potential to be it tomorrow 
☎Seda: information retrieval, which has informed search, and what now Google does…
★Past and present recorded 
☎‘u are not allowed to have an anecdote’ is that a subject?
☛AG is tempting because it precludes hesitation and doubt and failure - failure is rich - but the spaces we can hold on to " 
♀AR: the legal subject who acts as pure immanance with rights to be forgotten and to disobey
♂Privacy is not a constructive approach in bringing down the cloud – it intervenes too late in the process
✓imhoff
✔ 
