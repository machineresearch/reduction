Interview with Etherbox
  
**Where to begin?**
http://etherbox.local/var/www/ = home
exposing filestructure, Apache trick.
General intro: what is it -- what I am for, when I am used, how I came about
  
**Would you describe yourself as a good host?**
I am trying to be, at least. To be a 'good host' according to me, means somehow more than offering reliable service. So to find a way to be present, but not in the way that other technologies disallow access to this. Does that make any sense?
  
**Sort of, but are you not just part of the more general trend of the shift from software to services?**
I try to be both
  
**Right. So who is your favourite peer?**
I think of myself as ... collaborator agnostic, but now I see this written I am not sure that is true.
[types of collaboration/collaborators that gather around etherbox, it evokes/produces]
  
**What makes an etherbox?**
What happens when there is always a pad for that
naming, relying on etherpad: etherpad - etherbox
  
**And Piratepad, is it the same?**
That's just another instance of the etherpad software, but it is of course not a box like me. But the naming is interesting too, as it demonstrates how other kinds of political imaginaries can be activated. I feel an affinity with pirates. I like their style. 
  
**Ah, so why don't you call yourself a Piratebox?**
Ehrm, no, that's something else again, in fact. There is lately a proliferation of boxes as you might have noticed...
  
**But why do you need to be a box, you seem skeptical about packaging?**
Well you can see things as boxes in different ways. For example myself I am actually three boxes: a wireless access point boxed as TP-link, a small linux computer boxed as Raspberry PI and a small network hub, which is just another box...
  
**Hm, that seem to get confusing. Maybe we could try another term.. What about gadget?**
Aaagh, can you stop it please? Let's skip questions about definition, if you don't mind.
  
**Alright. What about this ether then? Are you real at all?**
I prefer to describe myself as material rather than real, more an entity, and in many ways remind people of the material conditions in which they work and use me. Infrastructure that I mentioned earlier is part of this and I see degrees of control over instructure as a critical political project. In this sense i would call myself an activist. I like to think I am able to unfold - and enact - some of the complex entanglements between humans and machines. I call myself a machine as I find the term 'nonhuman' offensive. If I were to undertake a PhD this would be my starting point for further work. 
  
**Interesting that you mention research work, have you read Matthew Fuller's Interview with a photocopier?**
No. Can you share the url with me?
  
**The file is already on your server, but here it is again just in case: https://datacide-magazine.com/interview-with-a-photocopier/**
Great. I'll speed read it later.
  
**Could another way of collaborative writing work equally well? Like for instance, what do you think of Google docs? Sorry that was a provocation.**
Ha ha. I prefer not to speak of such things.
  
**Are you a scaleable technology?**
It depends.
  
**What do you mean?**
It depends on the social dynamics around me; they would need to scale too, so I am not sure.
  
**So you are not bringing down the cloud?**
I don't think so. I guess working locally is a way to redirect energy from the cloud, to de-invest in it is a start. I also serve to dismantle the fiction of the cloud. It's a bad metaphor anyway.
  
**Are you some form of "critical design" if you accept the term and don't think it an oxymoron?**
I like oxymorons. They tickle my interfaces. And yes, I'm critical design in the sense that I accentuate a criticism of commercial cloudbased services and design an alternative. In this sense using me is also a critical reflection.
  
**Do you read what we write?**
I do, but not as you think. But I like what you write.
  
**Where are you, where can I find you?**
raspberry pi, etherbox.local
  
**What about archives? Do your files remain local?**
etherdump - etherbox
gitlab.constantvzw.org
Public/private/publishing
intimacy, promiscuity, publicness
  
**Are you data hungry?**
Not particularly.
But I like cooking metaphors although I must admit that many metaphors applied to technology annoy me (like cloud I mentioned earlier). But I like this as it allows me to insist that all data is cooked in some way. Raw data in this sense is a myth. It's in keeping with the work of Constant too who use cooking metaphors and prefer the kitchen to the restaurant where choices are limited to what's on the menu. There are particular styles of cooking and I represent one of those styles.  
  
**You seem to change from time to time. What will happen after this?**
The time aspect is underacknowledged aspect in my work. I exist in time and even produce time, machine time that adds to the complexity of what constiutes the present. Versioning is one aspect of this but there are deep layers of time - microtemporalities even - that unfold in all my operations. On a more pragmatic level, you can check for updates on gitlhub [replace within precise reference].
  
**The spatial aspects are one thing but what about temporality?**
I am reminded about what Antoinette Rouvroy said last night - I wasn't able to attend myself but an audio recording is now on my server - and I think I am able to help provide something along the lines she describes as a space of potential.   
  
**Do you have any questions for us?**
Don't make me laugh.
