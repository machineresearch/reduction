wpsrc=$(shell ls wordpress/*.wordpress.txt)
plain=$(wpsrc:wordpress/%.wordpress.txt=etherdump/%.plain.txt)
txtfolders=$(wpsrc:wordpress/%.wordpress.txt=txt/%/)
wpcopy=$(plain:etherdump/%.plain.txt=txt/%/wordpress.txt)

plaintext: $(plain)

txtfolders: $(txtfolders)

wpcopy: $(wpcopy)
 
all:

etherdump/%.plain.txt: wordpress/%.wordpress.txt
	filters/plaintext.py < $< > $@

txt/%/wordpress.txt: etherdump/%.plain.txt
	cp $< $@

txt/%/: wordpress/%.wordpress.txt
	mkdir $@



print-%:
	@echo '$*=$($*)'
