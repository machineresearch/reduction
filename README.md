*From 2000 to 1000 words in many machinic ways*

---

From 24-26 October 2016, Constant hosts the research/Phd workshop *Machine Research* in collaboration with the Participatory IT Research Centre, Aarhus University and Transmediale art & digital culture, Berlin.

The workshop contributes to the transmediale festival programme for 2017. Participants develop texts in seminars and talks in Brussels, generate off- and online publications, and contribute to public presentations at the festival in Berlin. 

The 2017 transmediale festival focuses on the elusive character of media and technological change and how it is articulated in the contemporary moment of messy ecologies of the human and non human. It explores perspectives of the nonhuman that suggests a situation where the primacy of human civilization is put into a critical perspective by machine driven ecologies, ontologies and epistemologies of thinking and acting. 

[Read more...](https://machineresearch.wordpress.com/about/)
