Presentation

"thinking with images" / images as thinking devices.

different approach than paper, complentary vision after the digital
start: Swedish government ready to put land registry on block chain
Land as property is redefined
Mapping unused resources
land as something belonging to someone is changed to ...
geohash type algorithm

grid, colonisation of the americas
would be populated by future generations, possibility of writing empty spaces
 cfr digital writing memory
how is soil a media-device

"inner colonisation" during government of Franco
new areas that would host workers of military & extractive industries
Eyal Weizman: 
in the colonial imagination
(and a forensic architecture approach more broadly that looks for evidnce. soil give a kind of testimony)

lobying power to transform territories

medianature (Parikka): an ecology of practices
land becomes a film of marketable colour

Reading and writing the Earth through aerial photography ("seeing and producing the visible")

soil as surface to transform light into growing matter, and light reading techniques to interpret such a surface
precision agriculture - farm machinary responding to the wavelengths reflected and absorbed by the biosphere
(Agribotix, company of pix) 
the very notion of "making" is destabilised when bringing medianatures perspective in.

impossibility of an on-off switch 

blurred differenciations between function/disfunction

Comments & questions

Maja: Techniques used in archeology, rediscovering those techniques. Discovery and re-discovery
Abelardo: technologies as landscapes. How the media-layer introduces temporality
Geoff: curious about link to Fanon's Wretched of the Earth, you mentioned in the beginning the "Wretched of the Screen?" by Hito Steyerl.
Abelardo: This process - laboured by prisoners of war "bringers of the green pixels" (wow)
Brought to you by people from the village: Stream of light = "Arroyo de la luz"

experiments of rational architects, briging the inside outside.
removing distance between soil/content & workers/users, cfr FB
<- analysis based on radical symmetry (which is not power horizontality...)
narrative infrastructures: same water grid used to feed people / vegetables

About the on-off switch. landscape design becomes real and it's not just a representation.
Holland: very cutlivated landscape / architecture for living > this can be traced back to successive rounds of land consolidation between farmers.

SørenP: Landscape architecture as a way of living, perhaps not only colonization?

colonization cases also interesting that happened meanwhile: Kibbutz for example. -> shared pressure/plan of inhabiting otherwise
 nitrogen to 
 
Brian: relationship with climate change (aka "nonhuman resistance")? clouds intervening in satellite images? weather disruption of pictures.
Carnegie Mellon's Terrapattern: Kyle McDonald and Golan Levin.  http://www.terrapattern.com/about
Karolina Sobecka
terra/terror patterns

Had to think (more for the title maybe than the content) about Scott's Seeing like a State
http://politicalscience.yale.edu/publications/seeing-state-how-certain-schemes-improve-human-condition-have-failed
Wondering about the difference level of resolution and error between that high modernism and cybernetic capitalism..

Farmers being the ones who speak cloud language

going here next: http://etherbox.local/pad/p/rosa-menkman
---
title: Abelardo Gil-Fournier – Unmaking screens. A genealogy of the mineral vision
slug: abelardo-gil-fournier
id: 93
link: https://machineresearch.wordpress.com/2016/09/26/abelardo-gil-fournier/
guid: https://machineresearch.wordpress.com/2016/09/26/abelardo-gil-fournier/
status: publish
terms: Uncategorized
---

The earth and the digital


The Swedish Land Registry recently announced tests being conducted to put the country’s land registry on blockchain [1]. This procedure encodes land by fragmenting it into blocks whose transactions -purchases, clearings, reclassifications, etc- are systematically traced, encrypted and recorded in chains stored in distributed databases, as do bitcoin operations. In the context of the digitization of the surfaces of the earth -through, among others, satellital images, aerial photogrammetry, sensor networks and GIS services- this decentralized protocol reinforces a topological approach to land, characteristic of infrastructural planning (Harvey 2012), which renders topography redundant: once locations are figured out from time delays in the communication with orbiting nodes (GPS), or addresses reinvented as algorithmic hashes (geohash, what3words), the old modern, metric-driven earth is replaced by a different, database-like entity.

This replacement, however, is not the result of a spontaneous transformation. The observation of the earth through media has occurred along with the expansion of the military and extractive industries of the 19 and 20th centuries, in hand with "the rise of an imperial world view" (Kaplan 2007). "In the colonial imagination", in Eyal Weizman words, "the planet is perceived as a design project" (Prochnik 2015). A project where the infosphere controls the geosphere (Virilio, cited in Bishop 2015), with encodings such as "the gridding of time and space, the proliferation of registers, filing and listing systems, the making and remaking of categories, the identification of populations, and the invention of logistics" (Lury, Parisi, Terranova 2012) that bring materially the planet to "behave topologically". A transformation led by operations such as depth soundings, cable laying, aerial photographs from balloons or terrestrial photogrammetry, that have inscribed physically in the earth their own infrastructural needs and their lobbying interests, expressing their "power to transform, redefine and hybridize nations, territories and cultures in a most material way", as Lisa Parks has put it in relation to satellital infrastructure (Parks 2009).

Furthermore, this relation between abstract encodings and material portions of the earth is a two-way one, as Jussi Parikka argues in A Geology of Media. It is not only that the earth as a resource has been registered through media for a long time; the registering tools themselves have been provided and enabled by the earth, in the form of essential chemicals, minerals and microorganisms mainly. A closed loop, a "double-bind" (Parikka 2015a), characterizes the interweaving between the planet and the technical mediations that allow to grasp it as a readable entity. A sphere of "medianatures" emerges, in his words, as the entagled set of practices where media and nature appear as "co-constituting spheres, where the ties are intensively connected in material nonhuman realities as much as in relations of power, economy, and work", making it impossible to distinguish such spheres separately. Medianatures trace thus how any act of media affordance involves terraforming operations and, reciprocally, how the histories of media are blurred with the long history of planetary geology, introducing the deep time scales of millions of years into the encoding practices such as the ones developed by the Swedish Land Registry.




Mineral Vision


The introduction of a cadastre into a blockchain pertains then to a larger set of practices of both digitization and geoengineering of the surfaces of the earth. Being vision the dominant way to sense surfaces, the question is now whether vision itself, besides its role within the extractive industries, becomes something else once visual media techniques are considered from the point of view of the entanglement with operations on the soil. "To engage aerial sightedness -or even vision in its most basic form-", writes Ryan Bishop, "is to yield almost completely to the promise and problems posed by the surface", and to rely thus "upon some other entity, some other ground, not visible or graspable for its support" (Bishop 2011). Vision inherits in its historicity and technicity the issues and nuances linked with the ways and contexts where it is produced and communicated, a ground of practices and operations that, without dealing directly with the visual contents themselves, belong to the relationships between humans and with the environment. We are born to see in a world not only immersed with images and practices of cultural reproduction, but filled also with displays, synthetic colors, or computer-based arrangements. Vision is grounded physically on the material flows of a factory system, lately dominated by "the drive by technoscience to extend the power of the senses, particularly vision, which has been appropriated by military and corporate sectors in the name of defence, health and entertainment." (Bishop 2011)

Writing about the practices and methods used to depict with and about light phenomena, Sean Cubitt recalls an analogy posed by Descartes, where light rays are compared to the precision of the stick used by those born blind, that allows others to almost say "they see with their hands" (Cubitt 2014). The metaphor succeeds at bringing to the foreground the materiality of a specific regime of vision, the solid cane tapping and modifying the ground at each act of vision. In a similar way, other analog sticks might be devised: in between the air-ground reciprocity, for instance, that drones increasingly reinforce when bombing autonomously targets (Adey 2011); or within the statistics of online audiences, where the views of contents -known sometimes as hits- are registered individually on a hard drive. The analysis of those mediated visions, however, does not finish at the close attention to the tools or devices. "Media materiality is not contained in the machines", argues Parikka, "the machines are more like vectors across the geopolitics of labor, resources, planetary excavations, energy production, natural processes from photosynthesis to mineralization, chemicals, and the aftereffects of electronic waste" (Parikka 2015b). Vision, meant to operate extractively, entails an industrially extracted past. It is a mining, mineral vision.




Unmaking. Practice-based genealogies


All of this makes it necessary to approach the contemporary regime of vision from the perspective of the procedures that contribute to forge it from this materialist point of view. That is, to introduce in the discussion and practice the technical terms, scales, resources and processes that build up media devices -for instance- in order to approach them critically. As a matter of fact, under the umbrella term of Critical Technical Practice, several authors, artists and engineers have been for long elaborating approaches such as the Critical Engineering Manifesto -by Julian Oliver, Gordan Savicic and Danja Vasiliev-, Mary Flanagan’s radical game design, Garnet Hertz’s critical making or Lori Emerson’s practice-based media archaeology [2]. Several of these, however, have put the focus on the techno-political literacy through the exposure and alteration of tools, devices or procedures; to address instead the wider spektrum of materialities here considered, broader genealogies would need to be taken into account.

In this direction, to understand the interweaving of materialities and temporalities underlying the mediated and codified vision that characterizes the visual nowadays, a genealogy of this extractive scopic regime is being proposed based on a case of landscape transformations linked to the expansion of industrial agriculture. Genealogical research is sketched in this case as a practice-based methodology, shortnamed as "unmaking". In its broadest sense, unmaking aims to explicitly introduce the processes of making media into the fields of forces and tensions characteristic of the non-binary worlds of medianatures. It is, n the end, a methodology that entails the question of how the very notion of making might be destabilized once it is put against this background of inherently interconnected agencies and scales.

The case in particular, which has been already presented with some detail elsewhere (Gil-Fournier 2016), deals specifically with the operations of the Spanish National Institute of Colonization, which was the Agrarian Reform and Land Settlement program of Franco’s dictatorship. During three decades, from 1939 to 1973, this Institute repurposed enormous extensions of territory topologically linked to the engineering of large-scale water infrastructures. As a consequence of it, agriculture in Spain started to be industrialized, thanks to the outcomes of chemical industries, on the one hand, and the disposal of cheap human workforce -a situation that was dramatically present after the war-. Significantly, this technification, exploitation and population of land coincided with the first series of aerial orthophotographic pictures mapping the whole Spanish territory. Soil became an engineered surface to hold and transform solar light energy into cereals, fruits and vegetables in the most efficient way, while at the same time the reflected sunlight became gradually a source of information to be a stored in photographic plates carried on by aircrafts owned by military and cartographic institutes.




Unmaking screens


This double performance of soil, commodifying the earth’s resources and emitting visual information, makes it tempting to extend the notion of a -digital- screen to encompass an envelope such as the uppermost crust of the planet, a move in some sense undertaken by Russian mineralogist Vladimir Vernadsky in the first decades of the last century, comparing it to a "living film" (Vernadsky 1998). Unmaking screens, rather than an program of direct critique to practices of design or making, or a call for dismantling or repurposing, addresses instead tools and devices as excavations and reformulations of the world, bringing to Critical Technical Practice a genealogical program.

To think the screen within the history of soil transformations renders singular episodes of the large scaled Green Revolution as significant to this discussion. As an example, a case of 58 families that left their homes in a village called Arroyo de la Luz -literally: "Stream of Light"- to migrate to a newly created town, part of the Colonization and Land Settlement program. Franco’s Regime had given to these families access to a new home and a plot, so they would become part of the human force employed to labor the soils. Coming from the Stream of Light, they were workers displaced to turn dry soil into green areas of agricultural productivity. From above, invisible to the images with a resolution tuned to monitor changes in the occupation of land, they weren’t actually distinguishable from the flow of other streams of light, water drops or nitrogen mollecules inside the factories of the photosynthesis. Hybridized inside a continuum of human and non-human agencies, the displacement of such "disposable people" (Bale 2012) recalls Hito Steyerl’s phrase to characterize the context of contemporary images, "the wretched of the screen".

The destination town of the workers, Vegaviana, is nowadays one of the most remarkable example of the rationalized rural architecture that was put to work during the years of the Colonization. Some of the urban plans of the new villages emphasized too an interweaving between population and soil. In many of them, such as Esquivel, the main square was placed in the outside limits of the town, as if crops were to be incorporated to the urban scene (Delgado 2013). Furthermore, the space between towns was measured in terms of a magnitude called the “cart-module” –the maximum operative distance covered by a settler with a cart- (Gaviria, Naredo and Serna 1978), being the centres of these circles of influence the nodes of the irrigation network, which provided with water to a shared grid of canals that fed both the soils and the settler’s homes. Settlers and crops were again not necessarily distinguishable, as if an underlying managerial grid were –using Bernhard Siegert words- addressing and symbolically manipulating both of them, as if they had, in consecuence, already been transformed into something else, into data.

The interweaving of users and systems, the impossibility of an on-off switch in the context of medianatures, the continuum between signals and data, the blurred thresholds between function and disfunction, operation and waste or the erosion of scale differences are some of the faded binary relations to work within the context of an unmaking methodology, such as the one here sketched.

[1] Reuters: Sweden tests blockchain technology for land registry http://www.reuters.com/article/us-sweden-blockchain-idUSKCN0Z22KV




[2] On Critical Technical Practice see (Dieter 2012)




References


Adey, P. et al. (2011) Introduction: Air-target Distance, Reach and the Politics of Verticality. Theory, Culture & Society. 28 (7-8), 173–187.

Bale, K. (2012) Disposable People: New Slavery in the Global Economy. Berkeley: University of California Press.

Bishop, R. (2011) Project ‘Transparent Earth’ and the Autoscopy of Aerial Targeting The Visual Geopolitics of the Underground. Theory, Culture & Society. 28 (7-8), 270–286.

Bishop, R. (2015) Smart Dust and Remote Sensing The Political Subject in Autonomos Systems. Cultural Politics. 11 (1), 100–110.

Cubitt, S. (2014) The Practice of Light: A Genealogy of Visual Technologies from Prints to Pixels. Cambridge, Massachusetts: MIT Press.

Delgado, E. (2013) Imagen y memoria: fondos del archivo fotográfico del Instituto Nacional de Colonización, 1939-1973. Ministerio de Agricultura, Alimentación y Medio Ambiente, Centro de Publicaciones.

Dieter, M. (2014) The Virtues of Critical Technical Practice. differences. 25 (1), 216–230.

Gaviria, M. et al. (1978) Extremadura saqueada: recursos naturales y autonomía regional. Barcelona: Ruedo Ibérico.

Gil-Fournier, A. (2016) ‘Flattening the Biosphere. The Green Revolution and the Inner Colonisation’, in 2016 London: Speeding and Braking: Navigating Acceleration. Online at academia.edu

Harvey, P. (2012) The Topological Quality of Infrastructural Relation: An Ethnographic Approach. Theory, Culture & Society. 29 (4-5), 76–92.

Kaplan, C. (2007) Vectors Journal: Dead Reckoning. Vectors Journal:. 2 (2), Available from:http://www.vectorsjournal.org/projects/index.php?project=11&thread=AuthorsStatement(Accessed 27 September 2016).

Lury, C. et al. (2012) Introduction: The Becoming Topological of Culture. Theory, Culture & Society. 29 (4-5), 3–35.

Parikka, J. (2015) A Geology of Media. Minneapolis; London: Univ Of Minnesota Press.

Parks, L. (2009) Signals and oil Satellite footprints and post-communist territories in Central Asia. European Journal of Cultural Studies. 12 (2), 137–156.

Siegert, B. (2015) Cultural Techniques: Grids, Filters, Doors, and Other Articulations of the Real. 1 edition. New York: Fordham University Press.

Steyerl, H. (2012) e-flux journal: The Wretched of the Screen. Julieta Aranda et al. (eds.). Berlin: Sternberg Press.

Vernadsky, V. I. (1998) The Biosphere. 1998 edition. New York: Copernicus.




