In this text, I share some preliminary reflections to my research work - mostly interested in the relation between language, matter and mediums, poetics and the question about which little sane has ever been written: the emergence of logos out of nature.

We usually take language as well as all systems producing expressions to serve two purposes, signification and communication.[1] The relation in which we hold these two functions, however, deserves discussion: can the two be co-dependent variables, completing and squaring each other? Can communication and signification be the two dimensions of the same problem or be otherwise connected in a productive manner? How would our current notions of communication, signification and medium have to change for this to be an actual possibility?

I raise these questions for the two disciplines of Communication Theory and Media Theory in its German declination, supposedly trafficking with language, seem to have been founded precisely on the possibility of splitting the two tasks and make them one the dependent variable of the other. This uncoupling is based on a reduction of the notion of meaning, operated through a tri-partite gesture of separation from the supposed medium, emptying out and flattening. For as much as committing to an idea of meaning comes with a variety of complications, a simple dismissal of it appears to limit expressions in equally undesirable manners (in other words, what is the function, associated with meaning, that we may need to maintain?).

Claude Shannon’s Mathematical Theory of Communication (1949) was answering to an engineering issue: putting into communication technical entities with different communicational capacities. Notoriously, Shannon’s solution to the problem was mechanistic, not in the trivial sense that it aimed at turning any content into discreet quantities that a machine could then compute, but because it was the consequence of (and contributing factor to) a mechanistic understanding of reality proper to first wave cybernetics, for which every system of communication ought to be constituted of an array of possible elements, whose sum, the unity, will always equal the linear addition of the parts.[2] We can say that, for this reason, to inscribe communication in a whole that is linear addition of parts, information was formalized in terms of probability.

In the attempt of achieving perfect transmissibility, any system is turned into a mechanistic reality within which the coding language and the content of the transmissible messages are conceived as separate entities. Of the two, solely the former actually matters to the system, which, in turn, structures the code in a manner that makes every message qualitatively commensurable to the probabilistic means of the signal of transmission itself. Indeed, from a mathematical perspective, mechanistic communication required the institution of a unity capable of handling two quantities at once: the number of messages that one could transmit and the number of ciphers made available to transmit any message within the same communication situation. The two variables were formalized as the same mathematical object: not only they were both discretized, but they were also both probabilistically defined.[3] Conveniently, once a coding system was in place, any mechanism capable of ‘agreeing’ on it could perfectly transmit messages and such ‘agreement’ needed to occur just at the level of the code, which was itself delinked from the messages it carried - hence the usual critique to Communication Theory and its deterministic vice. The code was arbitrarily matched with individual messages, of an arbitrariness that had little to do with Saussure’s arbitrary signifiers. In structural linguistics, signifier and signified do not exist as separate individual entities prior to their constitution in language. Signifiers are arbitrary mostly because they are distinguished via a process of self-differentiation, rather than opposition (Saussure, 120,121), and then combined with the signified, in a way that is as un-motivated as it is unreplaceable.[4]

Media Theory, in its techno-deterministic version,[5] is the other XX century discipline showing a particular interest for the materiality of mediums, transmission and storage. Discourse Network (1985) operates a shift, from literary theory to media theory, from talking about literature (and language), for instance, to simply talking about mediums. Kittler breaks with German hermeneutics and, more broadly speaking, with the legacy of German idealism, abandoning interpretation, understanding and ideas, now described as something merely contextual (meaning or noise swap place according to the context), which may as well fly away (205) whenever their medium-conditions of possibility - this new, odd type of transcendental - expire.[6]

Also in this case, the focus on mediums, the material components, storage or mean of transmission of a message or discourse, was moving hand in hand with a clear-cut distinction between the medium itself and that which the medium mediates or carries. This contributed to a notion of content, by definition contained, always in danger of being merely represented by the medium, which in turn is always in danger of merely representing something that is not itself. To be clear, in this configuration, content is mapped onto meaning and idea, whose fixity and origin are supposedly unjustifiable outside of an immanent materialist approach, and what is meant by representation is a trivial form of symbolization or standing in the place of something else that, if not deterministically explained, would be impalpable, transcendent. So formulated, the issue of representation does not render in its full scope a broader and more significant cognitive question that remains implicit in the problem of ‘standing for something else’. This hidden concern informs the debate over the necessity to maintain or abolish the distinction between appearance and reality. In the simpler formulation, instead, questions on the difference between appearance and reality are evacuated: nothing ‘stands for’ reality, either because reality cannot be located anywhere (not to say that it does not exist) or because appearance and reality have the same exact status, a motivation based on the assumption that appearance would otherwise be categorized as a lesser copy, with no role to play.

Ideas are simply determined by the mediums themselves precisely because no material reality can be taken to stand for something else, but is in fact operative, at work, the only existing force, active in its own right. What we come away with is a weird type of separation, in which distinct elements are unimaginatively deemed to entertain one of two possible rapports: either of representation or of determination.

As we know, mediums-run determinism does not merely state that historically connoted material conditions a-priori establish what is been mediated, it sustains that everything possibly belonging to a discourse is already embedded in the medium. Therefore, in the more interesting case of computation, the hardware is not simply the material that sustains the software, a hardware of electric circuits is what can articulate the software itself. The latter does not exist (Kittler, 1992), simply because it is embedded in the former. One may read this move as placing thought-like capacities (the procedural aspect of thought, the reflection that outputs a result not known a-priori, the autonomy proper to thinking) within material mediums. From the splitting of medium and idea to then get rid of the latter, we move onto a flattening of matter and thought.

In terms of the rapport between matter and thought, the situation profiled by Kittler leaves us with two options: a purely mechanistic thought and a matter capable of thinking, or at least acting of its own accord. If non-mechanistic thought or action are available options, they must be immanent to matter itself. Is this flattening maneuver the key to overcome the dual option representation/determination of above? The neo-materialist current embracing this approach - Jane Bennett’s Vibrant Matters (2010) offers one of many examples – would offer a type of medium of expression that is still largely insufficient. While finally locating agency (if not thought) in matter, an expansive medium not bogged down by its material restrictions, we sacrifice the possibility for intentional direction. In the case of means of discursive expression, we would sacrifice the possibility to direct expressions. Those would move acephalically in uncontrollable, often undesirable manners, effectuating events for which it will be hard to even approximate the site of responsibility and that would stand for a banal fetishization of novelty - and ultimately a new alibi for human-all-too-human responsibilities.

I suggest we intent a different route and re-think the relation between the tasks of communication and signification, starting from a review of the notion of expression itself and an inversion of the Kittlerean tendency. Husserl provides a working definition of expression (1913) that helps differentiating between what counts as language and what does not, but without reducing language to the merely linguistic. We can take language as the system capable of producing expressions, here defined as meaningful signs.[7] Intentionality qua meaning intention, indispensable to Husserl’s definition remains problematic, for it is native of phenomenology and eventually requires the institution of a transcendental subject. However, the intention holds together expressions pointing towards the special type of unity-separation taking place between sensuous signs and ideas (qua meaning). The expression constitutes a unity of non-identical elements, which are taken as irreducibly different, a disjunctive unity occurring precisely between thought and matter.[8] Taking Husserl’s as working definition, but without conceding the priority Husserl grants to ideas, we still need to account for the coming into being of communicable ideas.

Right before dying, Merleau-Ponty was trying to work on a metaphysical project, attempting to solve the problems that his phenomenological work had left unresolved. The locus of research was language, but starting from a rediscovery of Friedrich Schelling’s Naturephilosophie. Wanting to be very brief, we could say that Merleau-Ponty was trying to study the emergence of logos in relation to nature, and the idea he was working on at the moment of his death was the supposed missing link between the two: what he called the flesh, a term in which much of phenomenological thought still reverberates.

The flesh behaves as an element in the pre-Socratic sense of the term and as a connector that intertwines reality at the level of the inter-corporeal dimension - which Merleau-Ponty refers to as the inter-subjective dimension in his working notes - and the inter-corporeal with the incorporeal. Something in this double-movement reminds of the double function of language, communication and signification, but it is still insufficient to explain how both thought and ideas, essentially alien to nature, come about. While drawing this parallel, we shall inquire Schelling’s programme for a philosophy of nature – an attempt at reviewing Kant’s critical philosophy, grounding the transcendental in nature itself, from which to then comprehend the emergence of consciousness (rather than logos, and the difference needs to be stressed). Expectedly, this project comes with a deep re-conceptualization of what we can take nature to be.

Texts referenced:

Benjamin, Walter (1919). “On Language as Such and Language of Man”. In Selected Writing Vol.1. Translated by Edmund Jephcott. Harvard University Press: London, Cambridge, pp. 62-74.

Bennett, Jane (2010). Vibrant Matter, a Political Ecology of Things. Duke University Press: Durham, London.

Deleuze, Gilles (1969). The Logic of Sense. Translated by Mark Lester. Bloomsbury: London.

Frege, Gottlob (1892). "On Sense and Reference" ["Über Sinn und Bedeutung"], Zeitschrift für Philosophie und philosophische Kritik, vol. 100, pp. 25–50.

Halpern, O. (2014). Beautiful data. Durham, NC: Duke University Press.

Husserl, Edmund (1913). Logical Investigations, vol. 1. Translated by J.N. Findlay from the second German edition. Routledge: London, New York.

Kittler, Friedrich (1985). Discourse Network, 1800/1900. Translated by Michael Metteer and Chris Cullens. Stanford University Press: Stanford.

(1986). Gramophone, Film, Typewriter. Translated by Geoffrey Winthrop-Young and Michael Wutz. Stanford University Press: Stanford.

(1992). “There is no Software.” C-Theory: Theory, Technology, Culture, no. 32 (Oct, 18, 1995). Accessed online (Sept. 27, 2016), URL: http://www.ctheory.com/article/a032.html.

Kramer, Sybille and Bredekamp, Horst (2013). “Culture, Technology, Cultural Techniques – Moving Beyond Text.” In Theory, Culture & Society. 30(6), pp. 20-29.

Lyotard, Jean-Francois (1971). Discourse Figure. Translated by Antony Hudek and Mary Lydon. University of Minnesota Press: Minneapolis, London.

Merleau-Ponty, Maurice. The Visible and the Invisible. Translated by Alphonso Lingis. Northwestern University Press: Evanstone.

Nature, Course Notes from the College de France. Translated by Robert Vallier. Northwestern University Press: Evanstone.

de Saussure, Ferdinand (1916). Course in General Linguistics. Translated by Wade Banskin. Philosophical Library: New York.

Shannon, C. and Weiver (1949). W. The Mathematical Theory of Communication.University of Illinois Press: Urbana: Chicago.

Siegert, Bernhard (2013). “Cultural Techniques: Or the End of the Intellectual Postwar Era in German Media Theory.” In Theory Culture & Society. 30(6), pp. 48-65.

Wiener, N. (1961). Cybernetics; or, Control and Communication in the animal and the machine. New York: M.I.T. Press.

 

 

 

 

 

[1] Benjamin’s early writing appears to be one egregious example in which this double-function is given for granted.

[2] To be more accurate, Norbert Wiener’s dream was to get away with limited arrays of possibilities and produce a system for which, to every input message, a correct response – the element guaranteeing that the communication is effectively working – would come with no interval between reception, memory recovery and response (Halpern, 2014 and Weiner, 1948). Memory and perception would work in unison. A request that, if demanded from machines, appears to entail a mechanistic reality, not merely because a material object is doing the work, but more significantly because it requires the mutual flattening of material memory and material perception, returning a static reality conceived as a closed circle of immediate action/response, making no remainder available.

[3] By this I mean, once again, that the sum of the probabilities of all possible choices is established as the unit, hence we can deduce the probability of unknown choices, whose number may even be infinite, by linear subtraction.

[4] In this sense, Benveniste criticized Saussure’s use of the qualifier ‘arbitrary’ for signification, as also Jean-Francois Lyotard points out in his writing on language and sense (1971). From the point of view of experience, what Lyotard cared for in this context, there is nothing arbitrary in the uttering of certain words to signify certain images. The question of arbitrariness in the matching of signifier with signified and the fact that we can alternatively talk about arbitrariness or not, according to whether we look at signification from the perspective of experience or from the perspective of the formation of language, all this hides a broader set of questions that I hope to enquire in my research.

[5] Kittler’s influence seems to still be prevalent in the context of cultural techniques (Siegert, 2013; Kramer, Bredekamp, 2013).

[6] As David Wellbery puts it in the foreword to the first English translation, Kittler partakes with a community of intellectuals whose post-hermeneutics project is to stop ‘making sense’ (X).

 

[7] There is a significant similarity between Husserl’s definition of expression, with the distinction between meaning intention and meaning fulfilment acts, and Gottlob Frege’s work on logic and the notion of equality, which entails a distinction between sense and reference (1892). Both thinkers, working between the end of the XIX century and the beginning of the XX, were trying to account for language from the perspective of logic and made sure to separate what is considered to be the sense of an expression (which also Gilles Deleuze treats in a comparable manner in his book of ‘logic’, Logic of Sense) and the object of reference that, paraphrasing Husserl, ‘fulfils’ the sense in the ears/eyes of the listener/viewer.

[8] Another significant aspect to trace is the legacy of this unity-separation, or, possibly, synthetic disjunction in the writing of Albert Lautman and Gilles Deleuze.



























































































