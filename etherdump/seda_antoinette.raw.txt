COLLECTING NOTES BY EVERYONE
AUTOMATIC TRANSCRIPT OF SOUND RECORDING USING
Robert Ochshorn Gentle for automatic transcription
https://lowerquality.com/gentle/


Antoinette Rouvoy
Algorithmic Governmentality
The art of not changing the world


What  is the 'potential space' of algorithmic governmentality: "the present of AG is a poor present, it is already dead because it is not trembling with potentiality"


Optimized society 
We don't believe promise but services are so present that it changes us
We're indexed,  I change my behaviour 
Radical competition between fragments of Algoritmic persons 


escapes or senses
replacemnet of all we call categories, we are predisposed to management world
to be replced by digital reality by human intervention

blindness, make singularity of life…
aura of impartiality…
tries to map contradictions
new way of making the world predictable…. preemptive, available for action
vision of algo, creates new objects and spaces 

A.G. process of
art not changing world, closing digital on itself
anything that is not computational
it is immumization from life
abstraction of digital world

raw data: erasure, of singularity
being something else than the world…

what defines living is alteration
livig is to be altered

normativity to events in the world, completely immanent

Life used to be unpredictable, now it is predictable

used to be uncertainity as such

things respresented by data
science and things become the same

Big Data: crisis of representation


we would like to be able to grasp the world without mediation, the world speaks with degree zero writing of 1 and 0
speaking for us (9:00)


We want to grasp the world without mediation - this is big data. Assumes the world speaks about itself in the categories laid out. 

Governmentality is Foucauldian

opposite of truth regime…
particular force… by the wish to know

naming things, constitutes the law. But now, algorithms, we don’t have to testify, we are free not to tell anything…

subjectification:through avowal: process of subjectifcation (12:42)

algorithmic government: is w/o gov’t
body w/o event, alteration

disappear: the ability of critique
data mining, data visualisation

classical statistics: gave more or less a representation that could be criticized
(not enough data, not enough)

Big data: no selectivity: even noise is taken up and considered
profiles… not willing to be profiled
raw data pretends to be second skin of the world

optimisation of everything (regime where we now live)

big data: ideology and practice
amnesia of data (forget amterial conditions)

practice: it allows us naturalise whatever is captured

big data appear to store facts..
conditions, whoever knows them, changes them

not changing state of affairs: recommendation systems, data mining, naturalise sterotypes in society (this art of not changing world)

it pretends not to be constructed….
algorithms rule the world they are like gods…

not a truth regime, doesn’t have an event in order to arise!

liquidation of the event (best to prevent change)
digital marketing (personalised ad you receive things, don’t clicking is also information

drones: missiles are sent by data mining algos.. not people who are detected….
signature.. instead

no way to validify the model

immance of the algorithm…..
suppressing alteration

Big Data, Raw Data

38:00

Raw Data is very much cooked
de anonymizing data
de contextualising data

what is a data ( is result of the processing) you translate the physical universe

data is not voluntary transcriptions…

not everything is digital data
3 things to escape:

recalcitrance

things exist in digital form, so that the happen sometimes, 

the past (traces in art and utopias)
the future (against of excessive organisation)
organic life (we have bodies)

we are speaking bodies (we are incomplete) which is why we speak to each other

by nature we are schizophrenic, why we are uncertain beings

judges: has to decide…revision of law, it’s alive (full of bugs and corrupt, yes)

algorithmic governmentality is dead (computers fail a programme, not really fixed)


Raw data is cultural good 
Is very much cooked 
Anonymous data,  cleaning data,  formatted to needs 
Data prior of its processing does not exist 
Totalizing concepts 
It is not Totalizing,  only contains what is digital 
3 things will always escape : 
Utopias from past vs shrinked bog data reality 
Resistance to highly structured organisation,  organic lives are occasions for bifurcation 
Speaking bodies because we're incomplete,  not entirely in the present,  because of language 
It disconnects us from ourselves,  reason why wee uncertain 
Reason why judge can decide even if not clear - revision of our own production  (law)
Algoritmic governmentality is dead 

ethical beings….
Ethics available because we can doubt and we fail 

Transindividuation:
opens up a radically new space and objects…
we haven’t invented these politics for space…

"How does AG exist alongside broken, corruptible, totalizing forces -when in fact things are not always all on the grid and not everything is digitized. 
Four things escape and always escape digitalisation and these are the spaces to hold on to as resistance:

- physical things - the fact of bodies and organic life that is unpredictable
- utopias we had which didn't find a place in any present - 
- dreams of the future -  tenancy f life to be recalcitrant to organisation  - that;s what 
- If we were really present and complete we would not talk to each other;we are separated from ourselves through language anyway; 

AG is tempting because it precludes hesitation and doubt and failure - failure is rich - but the spaces we can hold on to " 


- algorithmic governmentality
- replacement of categories by something opaque but immanent to the real
- blind algorithms
- a new way of making the world preemptively available for action
- impression of "raw data" w/o origin -- rather, it's erasure
- data something that replaces the world, an immunization from life
- an immanent normality produced by the events themselves
- the target of algo-government is not bodies, but alteration _as such_
- it's a symptom of the criss of representation -- we cant bear "after-the-fact"
- big data implies that the world speaks of itself and us without categories
- it's a new truth regime
- subjectification -- the individual is who he says he is
- this is avoided with big data
- it's a government without subjects
- "old" statistics didnt have the ambition of replacing reality
- critique is what is left (?)
- there's a big difference between big data as ideology and practice
- ideology has to forget its materiality
- naturalizes whatever is captured
- not concerned with "reasons"
- reproduces _and_ naturalizes existing biases -> it's the art of _not_ changing the world
- occult status, big data dont have eyes but see everything
- DRONES WORK LIKE ADVERTISING
- precognition
- no grand truth
- the drone has as its target UNCERTAINTY AS SUCH


Q&A
- not so much a representation as the elimination of representation
- data is raw when it's in lack of interpretation
- rawification of data -> decontextualizing it, normalizing it for processing
- Maya asks, but is it really totalizing? there's the rest of the world
- so far data is inhabited by predatory forms
- how can we move the other way?

*
- doubts, hesitation, ambivalence -- this is what makes us ethical beings



Seda Gurses

Privacy and Engineering, computer scientist

Suspicion

why is Big Data unprecented?
Dutch Ministerie to her: what you said about the last 30-40 years….is invalid, b/c we now have Big Data

There is no software only services.

Fundamental change software (shrink wrap software) to services

The Crisis of Software
(field constantly changes)

We use the Cloud: Google.docs, Etherpad (part of another logic)

Software: used to sell it. Now they keep it, sell services

Waterfall model.

Looking at software engineering practice…

Why are we talking about big data, why algorithms?

privacy, security, etc.
Why do we use these lenses for critique?

privacy, discrimination and freedom? as responses?

We are going to bring down the cloud!!!!
Obfuscation of data (fills up teh cloud) that is not the tool…..

Order: History of Objectivity
Statistics
Data

not infrastructure of railroads, but sewers!!!! History of shit (dig in the ground)

Today: Microsoft (third quarter spike)
They bought Linked-In using debt…..

History of Shit: is about French language, demise and also the protection of Soveriegn

History of time-sharing, computing, and Waste…. wasted time was for debugging…

Data: is Nuclear Waste, it’s going to be around for a long time…
1:07

DNS: disrupted by bots (if IoT devices) baby monitor, consumer devices
Was it Russia or China? (he says targeting US)

cyber security discourse (needs to be unraveled)

ethics or big data

ethics is a space of critique…. notion of situated practices….

managerialisation….

what is the difference b/w ethics and politics

duty to govern…. to diverge and bifurcate

follow calculation, you are not ethical….
optimisation is not ethical

Question of accountability!!!!

you can have systems of reflexitivity done my humans

Census model: 2021 in UK
not just administrative, but asking people questions. This woud add robustness to the data (Rouvroy)

Phil Agre! (Seda mentioned his article I downloaded)

Ted Biefeld: looking at the History of vVsualisation!!!!!

Rethink a new social contract! (Rouvroy)

Cloud is just other people’s hard drives (Seda thought Søren said ‘house wives)

Rouvroy: algoithms (cannot do parallel calculations, it’s not performative, it’s not a language)

Seda: information retrieval, which has informed search, and what now Google does…

‘u are not allowed to have an anecdote’ is that a subject?




SEDA presentation 

(notes An)
The cloud 
Ideology and practice of.big data 
Suspicious why we talk about it now 

History of privacy and big data 
There is no software only services
crisis of software ever since it was born 
Use services in the cloud 
Software industry moved - you will constantly come to us 
Waterfall model for software development grew into individual as part of software process = agile programming 

Optimize software production I such a way that had as side effect, big data 

Privacy, discriminatie and freedom as only lenses 
Why think not using FB is political act 

What is the project : bring down the cloud
How? If it fills up the cloud, that's not the project 

95% thinks there is no histories 
Maybe look at different things ex history of sewers / shit (you need privacy to be able to bury your shit in ground, way to clean up french language )
Microsoft bought LinkedIn using debt while they have massive cash 
Industry of banks and co that invested in development of cloud and now they have to fill it 

Data as nuclear waste 
Radioactive toothpaste example 
Dns disrupted by machines of Internet of things 
Revolution with baby phones 

Cubersecurity conceptualized by the state 
China, Russia and Iran were ways to push things forward 
Dependence of companies 

AI ethics 
Or big data or ethics 

Ethics is space of critique between things and representación of things 



DISCUSSION

(Notes An)
Ethics : seeing what is behind the discourse 
Difference between ethics and politics? 
Ethics can be applied by politicians for ex sign Ceta 
Having courage to bifurcate instead of following the calculation / the program 

Ethics used to take conflict out 
Is rule based ethics 
Way of keeping uncertainty away 
Ethics takes risk to be wrong 
Which discourse on big data tries to keep us right and safe 

Experiment with million people and fail in ethical way? 
Question of accountability 
Cie speaks through human persons 

Solon Barocas local optimum 
Reroute trafic, to places where there is uncertainty 
Cfr autonomous cars not ethical now because people cannot be accountable or justify accident. There is no space to discuss this. Car will not hesitate 
Ethics needs a scene and time. 

Quantification was already there : 
Would you kill 10 people or 1? 
Symptomatic 
See and register where shifts occur 

History of crash testing moment where mathematics started to be understood as risk management 
Think differently about it on large scale 
Ethics as accountability and how does that apply to company? 

What is role of FB in stopping Trump 
It is not about 1 thing. It is being produced by different factors 

Agile software production 
Bring back sensors, asking questions to people and map big data onto that 
Taking process backwards :-)
Nice exercise 

Individuals could claim their data 
Run traditional and big data censors to see the difference 
Polyphonic perspective 

Debunk the myth of something 
Sts archeologies 
Protocol histories 

Philip agri 2 stories of surveillance 

http://www.tandfonline.com/doi/abs/10.1080/01972243.1994.9960162

Ted  Byfield,  History of visualisations 

Genealogies 

Antoinette Rouvroy


algorithmic governmentality - an art of not changing the world

a description of phenomena that escapes the senses
"immunisation from life" (against the future)
     against uncertainty as such 

desire for unmediated relationship to the world (non-representational)
     crises of representation
          because everything is present
attempts truth-regime (uncontestable), like the law also posits a factive truth
big data suppresses alterity

Foucault - avowal
     commitment

government without subject, a radical shift as data speaks for us
raw data pretends to be a second skin of the world
but is not raw but cooked

data seems to sugest that the world has spoken
things that begin to speak for themeselves and for us
that signs and symbols become the same is a symptom of a crisis of representation
     
removing the distance of real life to the norm
no selectivity is evident, even noise is incorporated in big data

objectivising the reproduction of the current state of affairs, making it impossible to contest
if this is the art of not changing the world - of closing the present as the spece of alteration - then what is the art of changing the world?
reenactment is a space for potential 


‘signature’ of a terrorist in a database
Present augmented to the future 
If you're terrorist today big potential to be it tomorrow 
Past and present recorded 


Seda Gürses

There is no software, there are only services that are now linked to agile development
what we learn from histories - crisi of softwrae that led to the field of software engineering
what gives rise to big data? how different poetntia spaces develop? why we talk about the things we do, what can we do?
in other other words, how to bring down the cloud? 
suspicion is required

we need to tae decisions on uncertain futures
perhaps waht we require is a new social contract


Discussion

rawification of data - decontextualising
language connects us to others but disconnects us form the self
     raw revises itself (autopoietic)
are there other potentialities here of acting with machines - inter-subjective relations or trans-individuation? 
imhoff

laporte's history of shit - a good reference to think about how language is cleansed in order to reinforce the power of the sovereign
and the management of waste and the implications for how we think about infrastructure

who is this us? 
AR: the legal subject who acts as pure immanance with rights to be forgotten and to disobey


Antoinette Rouvroy: “Algorithmic governmentality: the ‘art’ of not changing the world"
*Algorithmic reality not sensible to human perception
*Algorithmic govermentality not making meaning, but creating probabilities
*Algorithmic governmentality immune system / immunization from life
*The ‘art of not changing the world'
*Uncertainty at the core of algorithmic governmentality??
*Algorithmic worlding speaking for themselves – and for us
*Algorithmic dispositifs disposes us from truthtelling (cf. Foucault) – in big data we don’t have to utter anything as there is no subject
*Possibility of critique disappears
*Algorithmic profiling becomes a second skin to oneself, almost impossible to escape
*Dif. between big data as ideology and as practice
*Naturalizing whatever is captured as reality – appearing as matter of fact
*“The world has spoken” – why not allow digitalizing of life construct reality?
*Aim is not truth but optimization
*Event is defeated – whenever there is a counter-actualization of an event, the data of the event is diffracted and extracted from the event
*Alteration at the core of human life – algorithmic governmentality surpressing alteration
*Everything is always present – nothing can be re-presented
*Virtual potentiality is removed
*Law is life – algorithmic governmentality is death
*Need to invent new law of potential spaces and potential objects
*Projects without predictions rather than predictions without projects – project! (etym. = projection, throw forth) (projects, objects, subjects – subject as project, a matter of duration)
*Ethics is inherent in human beings only because we fail
*Facing the fact that we can be wrong is ethical
*What would ethics be in a system? Can a system be ethical?
*Question of accountability
*ME: would create a system of precarity in which the individual doesn’t wish to take any risk because it all falls back in the someone – go back to agency
*Is there a difference between making organisations accountable and making individuals accountable?
*Human rights in an era of algorithmic governmentality:
*The right to fabulate, the right to be forgotten, the right to express/account for oneself

Seda Gürses: “Histories of big data"
*Genealogy of big data – thinking about the different stories in order to understand why we suddenly talk about algorithms, big data, etc.
*From shrink-wrap software to services – end of software
*Instead of selling produced software and distributing it to people, the companies keeps the software and people come to them
*Data streams created through services
*Bring down the cloud
*Bringing down the cloud can only be down by tracing the genealogies of its existence
*“History of shit” – what is proper language, who is able to speak, etc.
*Waste: what is defined as waste? Excess.
*Privacy is not a constructive approach in bringing down the cloud – it intervenes too late in the process

                  
    
                  
    
                  
    
                  
          Antoinette Rouvroy        
    
    
    algorithmic governmentality     - an art of not changing the world    
    
    a description of phenomena that escapes the senses    
    "immunisation from life" (against the future)    
         against uncertainty as such     
    
    desire for unmediated relationship to the world (non-representational)    
          crises of representation    
              because everything is present    
    attempts truth-r    e    gime (uncontestable)    , like the law also posits a factive truth    
    big data suppresses alterity    
    
    Foucault - avowal    
         commitment    
    
    government without subject    , a radical shift as data speaks for us    
    raw data pretends to be a second skin of the world    
    but is not raw but cooked    
    
    data seems to sugest that the world has spoken    
    things that begin to speak for themeselves and for us    
    that signs and symbols become the same is a symptom of a crisis of representation    
             
    removing the distance of real life to the norm    
    no selectivity is evident, even noise is incorporated in big data    
    
    objectivising the reproduction of the current state of affairs, making it impossible to contest    
    if this is the art of not changing the world - of closing the present as the spece of alteration - then what is the art of changing the world?    
    reenactment is a space for potential     
    
    
    ‘signature’ of a terrorist in a database    
    
    Agovernmentality that escapes the senses    
    Areplacement of categories with something that is invisible but arguablyimmanent with the world    
    Anew world of making the world prepared for action and speculation – a new spacefor speculation and action    
    Animmunesystem of the digital, an immunisation from life, abstraction of thedigital from the physical world    
    Rawdata as something that replaces the world    
    Anormativity (Canguilheim) that is not different from the world    
    Indistinctionbtw things and data – signs and things become the same    
    è crisis ofrepresentation – we cannot bear differance anymore – the world speaks foritself    
    è a new truthregime? In fact the opposite. Dispositifs som ikke tillader vidner, we are freenot to tell anything, no subjectivation    
    A government without subject – evolving in real time, noevent, no interruption.     
    When signs and things become the same thing the space forcriticism disappears. Vs classical statistics, som kunne kritiseres.     
    Der er ingen udvælgelse (ideelt), selv støjen tager med,dvs. den tager alt med, selvom det singulære.     
    Ifbig data is the world, why criticise? Men stor forskel mellem big data ideologiog praksis.     
    Bigdata ideology er en slags amnesi – glemmer materielle betingelser –     
    Facts:Canguilheim: Facts appears to forget their conditions –     
    Istedet for truth -> operationality – a truth that doesn’t depend on anyevent – the liquidation of any kind of event    
    Thesuppression of alteration –     
    Putsus in a radical competition    
    Rawdata as oxymoron – raw data as the process of the rawification of data –     
    Bigdata is only about the digital:    
    thingsthat are not digital: things that have not happened (e.g utopias),bodies/organic life, the fact we are not completely contained, sprogetindsætter forskel -> tvivl –     
    Weneed a new law, politics and culture of the potential – reopen the space forimagination    
    
          Seda Gürses        
    
    There is no software, there are only services that are now linked to agile development    
    what we learn from histories - crisi of softwrae that led to the field of software engineering    
    what gives rise to big data? how different poetntia spaces develop? why we talk about the things we do, what can we do?    
    in other other words, how to bring down the cloud?     
    suspicion is required    
    
    we need to tae decisions on uncertain futures    
    perhaps waht we require is a new social contract    
    Relationsbtw dababases and big data –     
    agiledeveloping -> the disappearance of the subject-user -> becomes somebodythat is constantly in the development process, not somebody who speaks    
    Bringdown the cloud, if your project fills up the cloud, it is not the project –     
         
    
          Discussion        
    
    rawification of data - decontextualising        
    language connects us to others but disconnects us form the self    
         raw revises itself (autopoietic)    
    a    re there other potentialities here of acting with machines - inter-subjective relations or trans-individuation?     
    imhoff    
    
    laporte's history of shit - a good reference to think about how language is cleansed in order to reinforce the power of the sovereign    
    and the management of waste and the implications for how we think about infrastructure    
    
    who is this us?     
    AR: the legal subject who acts as pure immanance with rights to be forgotten and to disobey    
    
  
Links
