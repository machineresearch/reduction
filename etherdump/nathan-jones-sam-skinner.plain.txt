---
title: Nathan Jones & Sam Skinner – Relearning to Read: Speed Readers
slug: nathan-jones-sam-skinner
id: 99
link: https://machineresearch.wordpress.com/2016/09/26/nathan-jones-sam-skinner/
guid: https://machineresearch.wordpress.com/2016/09/26/nathan-jones-sam-skinner/
status: publish
terms: Uncategorized
---
WHAT ARE SPEED READERS?
Speed reading applications, which isolate and centre individual words from bodies of text, enable the visual system to reduce the number of saccades involved in ‘normal’ reading. When reading a word among many other words, for example a line of text, you are reading both backwards and forwards, scanning ahead to see what word is within your parafoveal vision, and back again. The speed reading app Spritz declares on its website that: “You’ll find that you will be able to inhale content when you regain the efficiencies associated with not moving your eyes to read. And you will no longer move your eyes in unnatural ways.” [Spritz]




A new natural then, where we inhale content, and exhale who knows what… Not so much vaporware, as vaping words. But the increased speed of reading is only one of the possibilities afforded by the processes involved. In fact speed reading as a term, application and a commercial enterprise, in the case of Spritz and others like it, has essentially appropriated and redirected the science of ‘optimal viewing position’ [Spritz] toward their own accelerationist ends. How timely that such phenomena is framed in terms of increasing speed and the productivity of the reader.

REREADING SPEED READERS
Torque has to date performed several applications of speed readers as art medium. Our first book, Mind Language Technology was exhibited at the Typemotion exhibition at FACT, Liverpool as a triptych: print book, ebook and speed reader. We used speed readers to provide fast information to gallery visitors during a residency at Tate on privacy and security. The machinic speed and aesthetic of the speed readers was suggestive of a machine reading and processes of text analytics that ‘reads’ and sifts though online activity for suspicious activity. 

And our second book was produced as a speed-reader video with animation, and exhibited at Furtherfield, London alongside a “slow reader” where visitors were recorded reading aloud poems appearing on screen one morpheme at a time.

We’re currently working with Tom Schofield (with whom we have developed an open source speed reading application) at Newcastle University’s CultureLab and neuroscientist Alex Leff to develop a range of new trajectories for rapid (and slow) serial visual presentation methods which “weird” this technology, and problematise the progression of reading mediums in general as being about acceleration. Exploring how we might re-learn to read across multiple formats, fast and slow, attentive and discursive. As a collaborative project, we are particularly interested in two distinct areas of research that speed readers intersect: Glitch Poetics and the design of type. Below we introduce aspects of this research, and close with some suggestions as to how our upcoming exhibition of Reading Machines at the Grundy Gallery, Blackpool will process these ideas as a range of artworks.

Glitch Poetics
Glitch poetics is a framework for reading how errors are captured, mimicked or induced to produce moments of “critical sensory encounter” with the technics of language. This perspective on linguistic error is influenced by the ways that glitches and malfunctions have been valourised in media arts – particularly for revealing the formally withdrawn materialities or cultural structures of black-boxed devices and softwares. But the glitch is a highly subjective categorization, and “new media” by their very newness, can also be said to constitute similar ruptures or breaking points in what appears to be fundamentally inaccessible. Our encounter with the world of new media, in this sense, is often indiscernible from the unsettling encounter we associate with glitch.

By cutting through the dichotomies of malfunction-function in this way, glitch poetics is a way to read the textualities of an era increasingly defined by the possibilities, and hedged-in by the limitations, of new media.

Speed readers offer the opportunity for such an operation. Speed readers have a strange relation to glitchiness, understood as wrong-functioning, as they are designed such that they induce trance-like states of heightened reading efficiency, and in a sense gloss over reading disorders that have been (controversially) described as “brain glitches" (Coles).  Casting the finite aspect of time in a day along with reading disorders as errors to be circumvented, speed readers are in a sense what we might understand as a make-do solution, a kludge of existing technics and concepts to produce a far from perfect bridge between circumstances.

Indeed, media archaeologist Lori Emerson included a proto-speed reader work by Young-Hae Heavy Industries among her candidates for glitchy e-literatures. For Emerson, the fact that a work such as BUST DOWN THE DOORS offers none of the linking, forking, freedom and choice we have come to expect from our avant garde hypertextual or Open Artworks (Echo), nor our multi-purpose personal technologies, renders them glitch – for instead we are forced to simply watch, or shut down. Whether or not this error of anticipation counts as a glitch, is one thing, but it is certainly interesting that the way in which Young-Hae Heavy Industries made-do with a javascript window produces its own rupture as a friction against the user, and the parallel technology of video.

The poet Caroline Bergvall is another artist who might be used to reflect on the speed reader and glitch. Perhaps the most recognisably ‘glitchy’ work in Bergvall’s ouvre is About Face (Bergvall), written as a pseudo-transcript of a reading Bergvall performed with a painful jaw. 



As well as engaging with the notion of the jaw as a technical apparatus on the same ontological plane as tape recorder and computer keyboard, the indeterminacies and faults in the text version of About Face demand a heightening of the reader’s subvocalisation mechanisms: the muscular response to reading in which we simulate speech in our vocal chords and tongue, and ‘say’ and ‘hear’ words in our head.

The Spritz speed reader conversely, subdues subvocalisations and eye movements because they are too slow to keep up with our newly augmented cognitive potentials. In the world as envisaged by this device, subvocalisation is not only not-necessary, it is an inhibiting factor on efficient communication. The gaps and disorders in About Face, by inviting us ‘back’ into Bergvall’s encounter with a faulty jaw, form a rebuttal or complication of the form of efficiency these kinds of device envisage.

On the flip side of speed reader invention, in a sense an artistic influencing ruptured by new media such as speed readers, we can observe works produced by Erica Scourti, who used to be Bergvall’s assistant.

Erica Scourti’s 2016 performance Negative Docs (Scourti).  tested her own speaking capacity against the speed reader itself. In this performance of increasingly negative extracts from her diary, Scourti’s speech quickly loses pace with the words as they flash on the screen, leaving the audience increasingly out of touch: both ahead of Scourti’s utterance and behind the text, drifting in the rift between; the split apparatus of sight, cognition, hearing and feeling operating at their limits. Read across the speed readers then, About Face and Negative Docs, are illustrated to be partner works, emblematizing the transversal nature of linguistic faltering and its potential to echo through systems of nerves, critique and technics (what Rosa Menkman refers to as a glitch’s “moment(um)" (Menkman)

Discussion in the field of electronic literature can often concern one or the other of these effects of technology: as techno-social reading context or means of production. Indeed, rarely is there a suitable opportunity to examine the way that a (relatively esoteric) technology becomes something that authors speak ‘across and through’. The relationship of Bergvall and Scourti across the rupture we call speed readers is particularly potent for such a reading.

I’m interested in the way our own work might take place within this rupture.

The Materiality of Type

Spaces of increased legibility and readability offered by the technics of speed reading enable the catalysing and disruption of other areas of the reading experience. Beyond simply increasing speed, new possibilities emerge regarding content, typography and the physical space we inhabit when reading.

Experiments with typography and speed reading in particular offer certain affordances to explore both the fundamentals of reading and to push it into more divergent or liminal territory. Investigating where the limits of legibility may lie, what machinic systems of computation and display may enable or replace, and how in turn this might affect our mediation of and with the world.







Images of Torque typeface accentuating areas where contour intersect to experiment with possibility and affordances of liminal reading.

 

[gallery ids="210,208,207" type="rectangular"]

Installation Typemotion, FACT - book presented ‘3 ways’ - print, epub, and speed reading.



(Above image courtesy of Stanislas Deheane, from Reading in the Brain, 2009)

Researching the phenomena of reading enables a kind of reading of the world beyond words, where text becomes a microcosm and interstice of other systems. For example, on a fundamental level why does text looks the way it does? Why does writing consist of such a number of strokes, arranged in such a way? And in particular where might the genesis of this lie? And within the context of this workshop, that attempts to explores more nuanced perspectives of how the nonhuman is put into a critical perspective by machine driven ecologies - how might text or the technics of writing be seen in machinic terms as an apparatus operating between worlds? And to take this one step further how might the machinic be driven by more fundamental exigencies of matter - where matter precedes agency, both human and technical? Can these processes, these machinations, be seen in terms of an engine at the heart of life, fundamental to and transferring energy between systems? 

Iris van der Tuin and Aud Sissel describe in their diffractive reading of Ernst Cassirer and Gilbert Simondon the “ontological force” of technological apparatuses. Writing that “what takes Cassirer’s and Simondon’s accounts beyond the terrain of relational and processual approaches, is their insistence on an irreducible third ingredient in the ontological entanglement: Technicity.” (Hoel and van der Tuin). Where “the human/nature mangle [is] essentially mediated by tools or technological objects.” (Ibid)


An instance of this co-constitution is suggested when we look at the evolution of language, tools and cognition. Where it perhaps matters less which came first as each co-constitutes and catalyses the other in a continual process of becoming (Ingold). Trading places and one in the other. Where each can be perceived as being as alive as the other. As such speed-reading can be studied as just another chapter in this process of differential re-becoming in a McLuhan-esque moment of ‘retribalization’ where the speed reader returns the subject to an animalist state of orientating through a landscape and recognizing objects within. As recent work by Mark Changizi has suggested - human visual signs possess a similar signature in their configuration distribution, suggesting that there are underlying principles governing their shapes. He provides an ecological hypothesis that visual signs have been culturally selected to match the kinds of conglomeration of contours found in natural scenes because that is what we have evolved to be good at visually processing. (Changizi) This body of research suggests that the words you are reading now look this way because they resemble the conglomerations of contours found in natural scenes, thereby tapping into our already-existing object recognition mechanisms. (Ibid)




The reading system synthesises external and internal world. Recycling both the natural landscape and our visual system to new ends. This neuronal recycling hypothesis implies that our brain architecture constrains the way we read.  We can consider it as a massive selection process, where over time, writers and designers have developed increasingly efficient notations that fitted the organization of our brains. (McCandliss, Cohen, and Dehaene). As Stanislas Dehaene argues, our cortex did not specifically evolve for writing, rather, writing evolved to fit the cortex and to be easily learnable by the brain. (Dehaene)


Thus in speed reading or optimal viewing positions, afforded through the machinic processes used to both analyse and evolve our reading systems old divisions between nature and culture fall away. 

Conclusion

So what is the role of Torque here as a public research project, and as people who are inherently sceptical of narratives portraying the history as a succession of ever-faster ever-more-efficient technologies? As ever, the answer is not to ignore this new technology, but to explore its embedded weirdness. We propose that the speed reader technology might indeed play a part in navigating contemporaneous evolutions in computational culture and new modes of reading. After all, the speed reader itself is merely one example, of a tendency for media to flow forward with little concern for the past. Twitter streams, 24 hour rolling news coverage and the notion of the status update - a new self every time - are other concerns of this. Another intriguing possibility is how persons might also choose to read faster without increasing quantity, and use the ‘saved’ or ‘new’ found time for another activity, a walk in the park or a bath for example. 

[gallery ids="204,203,202,201" type="rectangular"]

 

Clockwise from top left: Jermon Letvin author of "What the Frog's Eye Told the Frog's Brain" reading in the bath, scene from Clockwork Orange, Arctic trees, visualisation of parafoveal vision.

We’ve have observed our own tendency to become distracted while reading long form writing online, and this is a common complaint, but how was it any different before the internet? Plato famously decried writing for its potential ill effects on memory and verbal communication. Was there ever a time that was not like it is now? By producing our own speculative technics, we seek an alternative platform by which reading itself can be reassessed as a component activity of contemporary thinking and being.

 
Works Cited

Bergvall, Caroline. Fig: Goan Atom 2. N.p.: Salt, 2005. Print.
Changizi, Mark A., et al. “The Structures of Letters and Symbols Throughout Human History Are Selected to Match Those Found in Objects in Natural Scenes.” The American Naturalist 167.5 (2006): E117–E139. Web.
Clark, James J, and J. Kevin O’Regan. “Word Ambiguity and the Optimal Viewing Position in Reading.” Vision Research 39.4 (1999): 843–857. Web.
Coles, G. “Danger in the Classroom: ‘Brain glitch’ Research and Learning to Read.” Phi Delta Kappan 85.5 (2004): 344–351. Web.
Dehaene, Stanislas, and Research Director Stanislas Dehaene. Reading in the Brain: The Science and Evolution of a Human Invention. New York: Penguin Group (USA), 2009. Print.
Heavy Industries, Young Hae. BUST DOWN THE DOORS! n.d. Web. 4 Oct. 2016.
Hoel, Aud Sissel, and Iris van der Tuin. “The Ontological Force of Technicity: Reading Cassirer and Simondon Diffractively.” Philosophy & Technology 26.2 (2012): 187–202. Web.
Ingold, Tim. Tools, Language and Cognition in Human Evolution. Ed. Kathleen R. Gibson. Cambridge: Cambridge University Press, 1995. Print.
McCandliss, Bruce D., Laurent Cohen, and Stanislas Dehaene. “The Visual Word Form Area: Expertise for Reading in the Fusiform Gyrus.” Trends in Cognitive Sciences 7.7 (2003): 293–299. Web.
Menkman, Rosa. The Glitch Moment(um). Amsterdam: Institute of Network Cultures, 2011. Print.
Scourti, Erica. NEGATIVE DOCS (excerpt). Vimeo, 3 Oct. 2016. Web. 4 Oct. 2016.
Spritz. THE SCIENCE. 2016. Web. 4 Oct. 2016.


