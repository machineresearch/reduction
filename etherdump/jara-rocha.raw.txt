material for publication being cooked here: http://etherbox.local/pad/p/jara-rocha-for-graphviz
*
*Presentation
"I wasn't prepared for such a frontality"
southern english :-)

A complex infrastructure - South. 
infrastructure intended as a structure of management of life (not metaphorically, but material and machinic behavior. It is a fiction)

It can and must be rendered as a technological apparatus that facilitates certain semiotic-material flows, and not other.

South as a (political) fiction, but not a metaphor.

Possible situations off the grid
"the grid" is interesting way to referr to structurations of life.. as in Bookchin's "the social matrix of technology." 

Murray Bookchin - from The Ecology of Freedom chapter 10: Social Matrix of Technology
http://golibgen.io/view.php?id=761962
http://192.168.42.104/var/www/lib/Murray_Bookchin_The_Ecology_of_Freedom__.pdf

Need to separate between technology and technique

A political, theoretical urgency, including the need to confront the device of the "global Souths(s)sss"

What is a political fiction? 

Paul Preciado: South is a political fiction in the same sense masculinity is one.
alive regime

Colonial constitutions of certain parts of the body. Anarcha gland for example, an urgent renaming. http://anarchaserver.org/mediawiki/index.php/Anarchagland
https://anarchagland.hotglue.me/?rhizoma/

-> renaming these parts, this would be somatopolitical affection
in the same way: geopolitical fiction, glottopolitical fiction (asociated to accents and voice) oikopolitical fiction (care strike as social practise)

care-strike: suspending the current flow of productive distribution.
world can be operated with strong tactics of political fiction

it is not a 'goodist' tactic!

Disposession as a tactic,  Judith Butler and Athena Athanasiou.
Related to the governance of life. What constitutes South, ontologically/materially and relationally?
Ontorelational tactic

To inhabit, to render its infrastructure differently
Non-centric as a term.
Its also important to scale down into the intra - relational .... in the South.  (intra-actions: Barad)
Scaling up and down in the technological matrix of the South. 

changing scales, but in a non-identitary way
Relations to place of enunciation: gender, class, ability..., age, race...

As a tactic to operate on this infrastructure with the dispositf of dispossession i wanted to propose with you in a very concrete subject the use of language. 

Working with language, a cheap and affordable technology. Constants tool. It is a powerful tool to work together. 

Comments, questions

Kristoffer: You mentioned language as cheap technology. Many people instrumentalize language, where do you position your research within this discourse?
to call it a language, .. a tactical use?
South is written: we find it in bureaucratic documents.

Jara: South(s) is written in the bureaucratic frames, it can be heard in the vernacular modulations.

Code is language....
What is inside and what is outside of the South? What is North of the South? 
It can be found in different materialities. 
Cheapness => condition of possibility. Reverse-engineering: condition of Affordability. 

Geoff: Euraca, group in Madrid working with language & EU https://seminarioeuraca.wordpress.com/
South is fiction in crisis of EU and the euro (I mentioned Marazzi's Capital and Language https://mitpress.mit.edu/books/capital-and-language and Berardi Poetry and Finance http://www.selforganizedseminar.files.wordpress.com/2011/08/franco-berardi-the-uprising-on-poetry-and-finance-1.pdf - Finance in terms of speech acts)-have to check Austin again, here.
Jara: it is a poetry group, last days of the euro
attention for new conceptualists, attention to machinic writing and the depoliticization of the topics for the ways that were dealt with. https://seminarioeuraca.wordpress.com/programa3/
(Kenneth Goldsmith's Uncreative Writing.. )!
What implication would these techniques have when they attend the places of enunciation of the South 
Is language really cheap? is cheap the right word to use? (language - and literacy - is also very precious ). 
Maybe there is an alternative for 'cheap' 
preciousity and abundance can go together. It's interesting to understand language as an abundant/exuberant commons, and not only through the scarcity-based economic scheme.
In addition to Christian Marazz (who focuses on 'spech acts')i, I am also reminded of the works of Jacques Ranciere – who focuses more specifically on writing and its aesthetic/literary qualities. In his view this quality points to a condition in language – language as something “shared”, something not submitted to relationsof propriety and authority – where ownership and authority remain undefined. <-  a commons!, yes.

cf. austerity and poetry - Sam Riviere "81 Austerities"
https://www.theguardian.com/books/2012/aug/03/81-austerities-sam-riviere-review

-- I wonder about techniques: are you considering poetic-artistic operations on language, in which case the "mesostic" 'reading' of texts using the word South might be interesting... (the reference is to John Cage reading of James Joyce, using his name) 
and then also I wonder if Moretti's "Distant Reading" is a useful framework for this kind of research, to see how  South has been constructed historically? http://www.nytimes.com/2011/06/26/books/review/the-mechanic-muse-what-is-distant-reading.html?_r=0

language as written language, spoken language, coded, -- 

Maya: captivated by "South as a political fiction", what would the problem, reinscription or translation, or moving away from the geopolitical.. what are you trying to relocate? // I would like to cross Sara Ahmed here, with the approach on location-based language on Queer Phenomenology

Jara: Souths are not so tectonic. As a fiction it becomes available to be dis-mantled and retold.
South is not a geolocation. 
something that is there ontologically but that can be taken in fiction as a tactic. breaking with the fixed conditions of south in the geographical or subjectivity sense.

Maya: "I am supposed to come from the South, but I am actually from the East"
Working with Chinese scholar in changhai, reflecting on how India is to the west of China, but not for India, for the West.
West Heavens - "West Heavens" is how India is referred to in China
http://westheavens.net/en
http://www.e-flux.com/announcements/34835/you-don-t-belong-a-west-heavens-project/
Raqs collective www.raqsmediacollective.net and Sarai: www.sarai.net are collaborators of West Heavens on the Indian side

Jara: It is important to break the notion of "being" something
An: language as a political tool, where would Souths in terms of code? http://www.spip.net/ ? http://nas.sr/%D9%82%D9%84%D8%A8/ ?
Jara: We need to test this together. How is South en-coded? Literally. ie legal code, as in nation-state codifications. Scripting, re-writing, doing something to texts in relation to other political fictions. A constellation of codes.
Jara: It is not just about geographical Souths. It is also ways of reading, how new subjectivities can emerge.

SørenR: de-root archived history, turn to fabulation as imaginative continuous activity (Deleuze)
Interesting that it brings the issue of agency. Text-logistics is not just creative-literary practice, traditionally mono-directional.

South is a state of mind, http://www.documenta14.de/en/south/

The emergence of a new masculinity: Playboy along cold war. Constellations that construct a fiction
Pornotopia (Paul Preciado)

Christian: Speech-act theory. Jacques Ranciere. Language as a shared resource outside of ownership
Jara: Disposession! Language as a cultural commons

Kristoffer: the machine and the work of art

Nicolas: struck by you insisting on: "that is not a metaphor!" How to write fiction without metaphors?
Jara: if fiction can be found in code / can be codified, metaphors now are dangerous (it is not against methaphors as such)
How do you define infrastructure? (not tectonic) <- in my text, i define it as "any apparatus that operates at a certain scale and under certain conditions of standarisation." :-/ any other proposals to define it in one sentence?
reading/writing: interested in close descriptions than jump to metaphors
using language to reverse-engineer

Abelardo: many code languages are in English
Machinic brings its own exigences, have the North injected (such as English as a working language, the alphabet as a norm, etc). It is sitting in front, you have to cheat the machine if you want to embody the machine with other tongues.
If it is not a metaphor is it a metonymy then?


A Programming Language based in Arabic: http://nas.sr/%D9%82%D9%84%D8%A8/



---
title: Jara Rocha – testing texting South: a political fiction
slug: jara-rocha
id: 92
link: https://machineresearch.wordpress.com/2016/09/26/jara-rocha/
guid: https://machineresearch.wordpress.com/2016/09/26/jara-rocha/
status: publish
terms: Uncategorized
---
Terms and Conditions 

The term “South” brings a not-only geographically located nor a strictly territorial problematic: it invokes an ontological, constitutive and transversal construct, a structural management of life. Better said: South is infrastructural, if we consider any apparatus (Agamben: 8) to be infrastructural once it affects semiotic-material flows at a certain scale and under a certain regime of standardisation. 

Below, I will expose a selection of experiences that aim at identifying and unfolding simultaneous, intersectional enunciations, notations and dispossessions (Butler and Athanasiou) in relation to the specific apparatus of “South”. This opens up a plan for close-reading the management of flows to hopefully better understand the particular semiotic-material circuit which renders the so-called South and the lives and subjectivities that emerge and co-compose around it. With Penny Harvey and Hannah Knox in “The Enchantments of Infrastructure” I argue that through, with, within, along South there is a need to affirm and highlight the affective force inscribed in infrastructures, as it might hold “the promise of transformation”, “invigorated by mundane engagements with unruly forces that threaten to subvert the best laid plans of politicians and engineers” (Penny and Knox: 2).

I propose to attend to South with both inter- and intra- gestures: for the inter-Souths, the term could definitely be “pluralized” later on, perhaps assuming the proposal of referring to “the global souths”. South as a set of semiotic–material conditions of possibility, not as a unitary, universal, singular and fixed statement of truth. The plural univocity of that “set” may work for now, waiting for a proper reverse-engineering of its universalist rigidness.

Politics magnetizes around the conditions of possibilities. A politics of the possible implies to understand that its very key objective is the transformation of desire by accessible means. Fictional works are powerful techniques to widen desire in the shape of “the possible”. Fictions jump over the given - “the probable” – as imaginations that are expanded, projected, constructed, diffracted and cared-about. Often they function as proposals, other times as ready-to-go scripts and usually as hands-on instructables. They offer worldviews that might operate as blueprints for the immediate. And they can be quite affordable, too. Taking and applying fiction for affecting the conditions of possibility as a plan risks to be understood as a mere “goodist” proposal, almost naïve or only tactical. Quite differently, political fictions are at the fundament of the shared world we build on a daily basis. The Modern Project is one of the most evident and sophisticated fictions: operating collectively, unfolding along all its variations of techno-scientific and socio-cultural components. Political fictions have a leading role at the composition and adaptation of the possible in terms of their all-scale, all-durabilities, all-tangibility gradients of materiality, subjectivities and collectivities.

Thanks to meticulous descriptions like those of Paul B. Preciado (Preciado: 102), I understand that political fictions can definitely be alive. They tend to be alive. A political fiction that is operative is embodied, not alone, and it might exist in transition, in circulation: ready to be read and rendered. Here follow some found-alive political fictions that are at work -in their variety- as regimes of constitution, composition and production of the present presences:

        Somatopolitical fictions. Related to the flesh and its structuration along dichotomic organisations of health/pathology. E.g.: Anarchagland (reference below).
        Glottopolitical fictions. Related to the tongue and its modulations through grammar, syntax and diction -often articulated and regulated institutionally. E.g.: political-historical studies of Spanish made by José del Valle (del Valle: 88).
        Geopolitical fictions. Related to the modern regime based on the nation-state and the cientificist Greenwich imposition to order the world and define, modulate and sustain its transnational power relations as well. E.g.: the PIGS designation disseminated by The Financial Times in 2008 to refer to non-flying indebted territories (PIGS in muck).
        Oikopolitical fictions. Related to the productivist excesses on the neoliberal conditioning of life and internationally sexualized and racialized divisions of labor. E.g.: The care strikes (Preciarias a la deriva: 126)

As far as I remember, I have read and heard of the notion of political fiction in the South a number of times. But I have never known of approaches to the notion of South itself as a political fiction. If South is the infrastructural apparatus and fiction is the technique to operate and co-compose along it, I detect the urgency of experimenting South as a political fiction. An experimental urgency for which remembering might not be enough, and which might be not that far, neither: Intra-South fictions can and must be practiced presently, closely, accessively. We can afford that.

This is why I propose to keep experimenting with the cheapest, the most affordable technology: language! Where is the fiction of South inscribed, noted, noticed? 

Language as cheap tech

The proposal here requires understanding language as a technology for shaping the present... where speech, deed, writing and reading would be technical uses of it. Language is a way of sharing the present through new embodiments. Of letting go of the self and working on a common ground: it is a way of making world. Somantically, infrastructurally. 

Language: a technology that is not only low (as in battery but also as in passion) nor down (as in depressed), but also cheap. Language is cheap in the microeconomical sense: affordable and hence ready for placing radical micropolitics into practice; but also cheap as in promiscuous: dispossessed from the technocolonial scale of values, so contextually demanding.

The above exemplified listed regimes of presence never apply individually, but in complex compositions, entangled. In search of ”mundane engagements with unruly forces”, I wonder about the intersectional and transversal practice that could turn political fictions in a fruitful repository of possibles: What fictionalizations of the South could render other infrastructural compositions for the transitional, dispossessed and non-anthropocentric entities that undoubtfully could emerge from them? How can the relationship between language and subject be scaled up to one between language and world-making, problematising the celebratory anthropocentrism of language in a non-identitarist but situated opaqueness?

In order to inhabit this search, a selection of pre-existing practical experiences -all in a experimental stage- has been made, attempting to work from them in relation to the statements made above.

The perspective of machine-driven textual practices is key to help in the grasping of the opacities and complexities of present linguistic ecologies and their text logistics along the here-now ontological transitionings towards the non-identitary enunciations of the people to come. 

Not Yet Know: the syntax for surprise ontologies

In the context of a European project called “How to do things with documents”, the Antoni Tàpies Foundation in Barcelona opened up its institutional archive for a number of artists and collectives to operate with it differently, bringing their own research questions. Objetologías is a research group that is based on a shared sensibility towards the potentials of displacing the subject from the center of analysis to attend carefully to the politics, aesthetics, ethics and erotics that might emerge from that switch of the new materialisms (see blogsite reference below).

The Foundation's archive compiled a long-term documentation practice that gave account of the institutional apparatus from the very inside. Power relations in the art world (double-layered through the Catalonian scene and the international one) were kept in the shape of email, postcard, certificate and invoice exchanges.

For this project, Objetologías' crew decided to take a folder of the archive and operate two gestures over it: firstly, extracting the privileged subjectivities from its inscriptions (museum directors, curators, managers, recognized artists, loan givers); secondly, remediating the content mimicking certain manners of the modern institution as the monodirectional reading and the interactive device, plus an extra one: designing and programming a bot that would publish newly composed affirmations based on found footage from the archive's folder.

The bot took its name from a sentence found in the archive: Not yet know. It explicitly described its way of being in the project: re-composing the material to produce non-historical and hence fictional affirmations in regards of “what is there”.

What is most interesting about this project is its bet for surprise ontologies found through a machinic syntax exercise: spinning the rendering of the not yet, and the rendering of the no longer, as well. In collaboration with Interzonas Collective (Karlos G. Liberal et al.), and using the Markov chain model (reference below) as a basis to compose the algorithm using pattern recognition for new syntactic iterations of the archive, the bot certainly texted into many unknown knowns of Southern politics:
{
  "1447247123956" : {
    "fecha" : "2015-11-11T14:05:23+01:00",
    "frase" : "The semester, works of an available restoration. Works of a Spanish restoration. "
  },
  "1447256065095" : {
    "fecha" : "2015-11-11T16:34:25+01:00",
    "frase" : "Paint an image on reception. Did not succeed in making modern representatives. "
  },
  "1447256066562" : {
    "fecha" : "2015-11-11T16:34:26+01:00",
    "frase" : "Appreciate your Spanish interest. Once we have received it, retake the satisfactory notion. "
  },
  "1447261820571" : {
    "fecha" : "2015-11-11T18:10:20+01:00",
    "frase" : "Have fallen into the a readable routine. Keep eyes and ears opened to every control related to our subject. },
  "1447262166227" : {
    "fecha" : "2015-11-11T18:16:06+01:00",
    "frase" : "The last day of a splendid vacation appreciate your interest. After reading your postcard, are conceived as a multinacional spectacle. "
  },
  "1447262235087" : {
    "fecha" : "2015-11-11T18:17:15+01:00",
    "frase" : "Already approached barça. Decided that Catalonia was "I wasn't prepared for such a frontality" too particular and probably not too polÃ­tico-social. "
  },
  "1447262458997" : {
    "fecha" : "2015-11-11T18:20:58+01:00",
    "frase" : "Appreciate your three-dimensional interest. In mid-september the possibility of showing your cultural restoration. "
  },
  "1447262525006" : {
    "fecha" : "2015-11-11T18:22:05+01:00",
    "frase" : "Some time, the possibility of showing your polÃ­tico-social a basque. Are conceived as white spectacle. "
  },
  "1447262559094" : {
    "fecha" : "2015-11-11T18:22:39+01:00",
    "frase" : "Did not succeed in making satisfactory a restoration. Be associated to a an other soccer team. "
  },

Rendering the affront by pragmatics: the urgency for Euraca assemblages

Pragmatics encompasses speech act and other approaches to language behavior, bringing context to the front. In a cultural context fueled by revolt against imposed structures of so-called Spanish “democratic transition” (collectively problematized along the 15M momentum and apparatus), there is a gang in Madrid organized around a poetry and poetics seminar on “languages and langues of the last days of the €uro”: “Euraca is a laboratory of speech, of tongue, of deed, of language, of poetry.(...) (it) is an empowerment tool for inhabiting the southern territories, the rescued economies. It is a liberation technology for a non-identitary ecology of different agencies aiming to be definitely dispossessed from the imposed institutional corpus” (Rocha: blogpost). The gang's naming tactinc is to render the affront “sudaca” into the southern-european contemporary conditions, attempting to run away from strong identity compositions while at the same time attending the shared place of enunciation (see general “about” page on blogsite: Euraca 1). Participants, their literary canons and their accentuated dictions might be european bodily, but perhaps not so much willingly: the coordinates of austericide and precariat in a context of datafied citizenship where individuality is generated by governments -suffering from a neoliberal path dependency- and produces quite a different kind of subjectivity to that produced by previous regimes such as the sovereign and the biopolitical.

Quite interestingly, this update and placement of the gang's reading-writing practices assemble the sensibility for situated knowledges and vernacularism with a close attention to contemporary poetics. This brought Euraca assembly to a testing the texting experiment through the New Conceptualisms, the latest recognizable poetry wave characterized by its digital management of language masses and a non-human-centered “uncreativity” (Euraca 2). The test served only to confirm a strong need to keep taking care of an aesthetics in languaging practices that does not link the machinic intervention with a loose and depoliticized kit for language gamers. Perhaps this is no place to look closer at that, but the transnational discussion on poetics after after Kenneth Goldsmith's reading of “The Body of Michael Brown” (Caconrad: blogpost), evidences the harsh depolitization risk new conceptualist poets (mainly white, male and western) take in “becoming agents of disappearance, agents of harmonization of a 'provisional language', 'lowered' and 'transitory'”(Salgado 1).

Nevertheless, this field-trip into the New Conceptualisms confirmed the potential of questioning identity as a possible fundamental for the elaboration of critique and of, ultimately, common life. In other words: a reverse reading of the generally strong depolitization of the new conceptualist flows of language slides in a Euraca wonder (Salgado 1): may digital machinic procedures of text logistics still provide plausible coordinates for testing non-identitarist language-based practices that keep the sensibility for situation and difference in a contemporary literary practice informed by computerization?

Being suspicious about the supposed non-subjectivity of the machinic, Euraca still values any attempts of looking at language as a form that does not take shape exclusively nor centrally in relation to the human subject (let alone its engendered, racialized, ableist and other hierarchical readings), but as a powerful apparatus that affects the infrastructural building of a shared world.

Digital verbal materialities are not globally homogeneous: they differ in their displaced, evicted, transitional, eccentric materialities. And they invoke presences; produce a present. A number of questions emerge at this point: What implications would it have to test and text Euraca's sensibility in the machinically textualized South? How might we dispossess from authorship in relation to content and context while materially caring for the conditions of possibility that come with the tensioning of both the lyrical genius and the quantified self? 

“Dispossession can be the term that marks the limits of self-sufficiency and that establishes us as relational and interdependent beings”(Butler & Athanasiou: 3). In this respect: Is there any political potential in performing dispossession instead of more-known appropriation in machinic reading-writing practices? If so, what machinic procedures and methodologies could serve to let go of the self for an otherwise politicized pragmatic enunciation? With María Salgado, I agree on the potential of “providing ourselves with a growth based on losses” (Salgado 2) in the textualized rendering of the present. 

A text practice that is non-identitarist but is affected by situations contains the potential and perhaps also the urgency of taking the machinic -specifically in Southern apparatuses- with its performative variants in the political. To end with, I would like to copy-paste here some questions formulated by Athena Athanasiou in conversation with Judith Butler: “What happens to the language of representation when it encounters the marked corporeality -at once all too represented and radically unrepresentable- of contemporary regimes of “horrorism”? How does ineffability organize the namable?” (Butler & Athanasiou: 132)

Situated semantics for a collective thinking machine

“It is the assignment of the Darmstadt delegation to explore the techno-political and socio-emotional relationships between activist practice and tools” (Darmstadt Delegation). While dealing with its sneaky and transdisciplinar constrains for attending the divisions of labor and practices of delegation that occur in times of crisis also in the techno-political sphere, the gang of The Darmstadt Delegation attempted to directly work with the vocabularies unfolded for a collective analysis of the problem regarding the aspects of 'time', 'separation' and 'labor'. From a collection of situated anecdotes shared by the participants on a public pad, a workshop hosted by Schloss Solitude in October 2015 (reference: “Re-Constructing Authorship”) used Graphviz software (reference below) to test the texting under a radically immanent diagrammatisation. 

http://etherbox.local/var/www/txt/jara-rocha/image1.png
[caption id="attachment_323" align="aligncenter" width="988"] Snapshot: The Darmstadt Delegation, 2015[/caption]

“This labyrinthine graph is not a data-visualization, but a thinking device: It is a playful text that is readable but also writable and executable by a number of agents. Lines, colors, and contours act as a semiotic gesturing of thought, generating multiple promiscuous concepts.” (…) “It distributes items diagrammatically and automatically over a surface, attempting to minimize the amount of crossings and scaling to fit. The graph mediates a sneaky kind of thinking that keeps on changing position, re-orients the conversation on purpose, directs the reader, and willingly loses the stream of recognizable, hegemonic, artificial, and collective consciousness.”

As an experimental practice, the use of graphviz as a tool for disposing semantic fields, hints at the possibility of a political, aesthetical and ethical machine-related research that is truly generative and that brings into a common plane of symmetry the differences of situated knowledges and experiences of all participant agents.

Prospect 

This brief and open selection ef experiences in itself wishes to work as a proposal to continue the testing and texting of South as a political fiction affecting, attending, processing, writing, reading, saying, assembling and directly operating its places of enunciation and its modes of existence and survival. It can be taken as an invitation to a collective languaging experiment for which declarations, questions, code, assertions, calls, certificates, manifestos, applications, invoices, memories, formularies, constitutions, exams and testimonies might be apprehended as raw material for testing and texting political fictions. For that, I keep suspecting that machine research can and must provide the material conditions of possibility for wild combinations and/or unsuspected renders. From the very South, with love.

_________________________________

Works cited:

        Agamben, Giorgio, What is an Apparatus? Stanford: Stanford University Press, 2009. Book.
        Anarchagland website: https://anarchagland.hotglue.me/?glandulas
        Butler, Judith and Athena Athanasiou, Dispossession: The Performative in the Political. Cambridge: Polity Press, 2013. Book.
        Caconrad, “Kenneth Goldsmith Says He Is an Outlaw”, https://www.poetryfoundation.org/harriet/2015/06/kenneth-goldsmith-says-he-is-an-outlaw/ Blogpost.
        The Darmstadt Delegation. http://snelting.domainepublic.net/category/affiliation-cat/darmstadtdelegation Research group.
        Euraca 1: “So called Euraca” https://seminarioeuraca.wordpress.com/so-called-euraca/ Blogpost.
        Euraca 2: “Conceptualismos Yankis” https://seminarioeuraca.wordpress.com/programa3/ Blogpost.
        Graphviz. http://graphviz.org Software.
        Harvey, Penny and Hannah Knox, “The Enchantments of Infrastructure”. Mobilities 
        Vol. 7, No. 4 (2012) 521–536. Article.
        How to do things with documents (Fundació Antoni Tàpies, 2015). Exhibition. http://www.fundaciotapies.org/site/spip.php?article8380
        Liberal, Karlos G. et al., Interzonas Labs http://labs.interzonas.info/ + https://github.com/karlosgliberal/tapies 
        Markov chain model https://en.wikipedia.org/wiki/Markov_model Wikipedia entry.
        Objetologías (2013-). Blogsite. http://objetologias.tumblr.com
        PIGS in muck (2008), Financial Times' Article quoted and commented: http://www.spanishpropertyinsight.com/discussion/forum-topic/pigs-in-muck-the-ft-article-which-shocked-spanish-media/
        Precarias a la deriva, “Precarización de la existencia y huelga de cuidados”. In: VARA, María (Coord.) Estudios sobre género y economía. Madrid: Akal, 2006. Bookchapter.
        Preciado, Beatriz, Pornotopía. Arquitectura y sexualidad en Playboy durante la guerra fría. Barcelona: Anagrama, 2010. Book.
        ___ , Las subjetividades como ficciones políticas. Conference at Hay Festival. Cartagena: 2015. Accessible (in Spanish): https://www.youtube.com/watch?v=R4GnRZ7_-w4 
        (Re-)Constructing Authorship (Schloss Solitude, 2015). Workshop. http://www.akademie-solitude.de/en/events/~no3764/ 
        Rocha , Jara, “Mutual rescue: the urgency for euraca assemblages” (2015). Blogpost. http://jararocha.blogspot.com.es/2015/10/draft-mutual-rescue-urgency-for-euraca.html 
        Salgado, María, “La escritura no creativa de Kenneth Goldsmith y la ola conceptualista” (eldiario.es, 2016) Article.
        __ , “2006 / 2016 31 poemas”, http://globorapido.blogspot.com.es/2016/09/2006-2016-31-poemas.html. Blogpost.
        del Valle, José, “Lo político del lenguaje y los límites de la política lingüística panhispánica”, Boletín de Filología, Tomo XLIX Número 2 (2014): 87-112. Article.


