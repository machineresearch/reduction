#!/usr/bin/env python

import cgi, os, sys, glob, stat, re
from jinja2 import Template
import subprocess
import cgitb; cgitb.enable()
from settings import FILTER_PATH


method = os.environ.get("REQUEST_METHOD")
path = os.path.split(__file__)[0]
filterspath = os.path.abspath(FILTER_PATH)
tvars = {}
filters = os.listdir(filterspath)
# filters = [os.path.basename(x) for x in filters]

print('hello world')

def executable_p (x):
    fp = os.path.join(filterspath, x)
    s = os.stat(fp)
    return bool(s.st_mode & stat.S_IXUSR)

filters = [x for x in filters if executable_p(x)]

filters.sort()
# filters = os.listdir(filterspath)
# todo ... filter by executables
tvars['filters'] = filters
fs= cgi.FieldStorage()
tvars['text'] = text = fs.getvalue('text', '').decode("utf-8")
tvars['filter'] = filtername = fs.getvalue("filter", "")
tvars['fragment'] = ''

print(filters)

def autolink (txt):
    return re.sub(r"(?<!>)(https?://\S+)", lambda x: u'<a href="{0}">{0}</a>'.format(x.group(1)), txt)

if filtername:
    # Run with --help to get docstr
    # sanitize & ensure filtername
    filtername = os.path.split(filtername)[1]
    filterpath = os.path.join(filterspath, filtername)

    if not os.path.exists(filterpath):
        raise Exception("filter '{0}' not found".format(filtername))

    sp = subprocess.Popen(("./"+filtername, "--help"), cwd=filterspath, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    helptext, _ = sp.communicate("")
    tvars['help'] = autolink(helptext.strip().decode("utf-8"))

    if text:
        # print "running", filtername, filterspath
        sp = subprocess.Popen(("./"+filtername, ), cwd=filterspath, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = sp.communicate(text.encode("utf-8"))
        if stdout:
            stdout = stdout.decode("utf-8", "ignore") # adding ignore to ignore non-utf-8 chars
            tvars['fragment'] = 'stdout'
        if stderr:
            tvars['fragment'] = 'stderr'
            stderr = stderr.decode("utf-8", "ignore")
        tvars['stdout'], tvars['stderr'] = stdout, stderr

print "Content-type: text/html;charset=utf-8"
print
print Template("""<!DOCTYPE html>
<html>
<head>
    <title>machine research: text filter</title>
    <meta charset="utf-8">
</head>
<style>
body {
    padding-right: 2em;
}
.boxsizingBorder {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
}
textarea {
    border: 1px dotted gray;
    width: 100%;
    height: 240px;
    font-family: monospace;
    font-size: 12px;
    padding: 1em;
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
}
div.output, div.help {
    white-space: pre-wrap;
    font-family: monospace;
    font-size: 12px;
    border: 1px dotted gray;
    padding: 1em;
}
#footer {
    padding-bottom: 100px;
}
</style>
<body>
<form id="form" method="post" action="">
<div id="filterselect" class="filter">
    filter: <select name="filter"><option></option>{% for f in filters %}<option{% if f==filter %} selected{%endif%}>{{f}}</option>{% endfor %}</select>
    Drop new filters in the <a href="/var/www/filters/">filters folder</a>.
</div>
<div id="stdin" class="stdindiv">
    {% if filter %}<h1>{{filter}}</h1>{%endif%}
    {% if help %}
    <div class="help">{{ help }}</div>
    {% endif %}
    <p><a href="/var/www/filters/{{filter}}">View SOURCE</a></p>
    <div class="textareadiv">
    <h1>stdin</h1>
        <textarea name="text">{{text}}</textarea>
    </div>
    <input type="submit" /><input type="reset" />
</div>

{% if stdout %}
<div id="stdout" class="stdoutdiv outputdiv">
    <h1>stdout</h1>
    <div class="stdout output">{{stdout}}</div>
</div>
{% endif %}
{% if stderr %}
<div id="stderr" class="stderrdiv outputdiv">
    <h1>stderr</h1>
    <div class="stderr output">{{stderr}}</div>
</div>
{% endif %}
<script>
(function () {
    var form = document.getElementById("form"),
        select = document.getElementById("filterselect"),
        hash = "{{fragment}}";

    select.addEventListener("change", function () {
        form.submit();
    })

    if (hash) { window.location.hash = hash; }
})();
</script>
<div id="footer"></div>
</form>
</body>
</html>
""").render(tvars).encode("utf-8")